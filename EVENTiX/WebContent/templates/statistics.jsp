<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="management.*"%>
<%@ page import="events.*"%>
<%@ page import="locations.*"%>
<%@ page import="users.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ include file="tpl_header.jsp"%>
<% 
	if(session.getAttribute("status") == null || (Integer) session.getAttribute("status") != 3){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}
%>

<div class="mheadline">Statistiken</div>

<%	
	UserManagement um = null;
	String statistik[][] = new String[0][0];
	
	try{
		um = new UserManagement();	
		statistik = um.gibStatistik();
	}catch(Exception e){
		response.sendRedirect("index?fkt=error");
	}
	
	for(int i = 0; i < 18; i++){
		out.print("<span style=\"font-weight: bold;\">");
		out.print(statistik[i][0] + ":</span> ");
		out.print(statistik[i][1]);
		if((statistik[i][0]).toLowerCase().contains("prozent")){ out.print("%"); }
		out.print("<br />");
	}

%>



<%@ include file="tpl_footer.jsp"%>
