<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ include file="tpl_header.jsp"%>

<% 
	if(session.getAttribute("status") == null || (Integer) session.getAttribute("status") != 1){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}

if(request.getAttribute("date") == null || request.getAttribute("id") == null){ 
	response.sendRedirect("index?fkt=error&code=createevent");
}
	
%>

<div class="mheadline">Event Anlegen - Dateneingabe</div>
<% 
	if(request.getAttribute("error") != null){
%>
<div
	style="text-align: center; color: red; font-weight: bold; font-style: italic;"><%=request.getAttribute("error") %></div>
<%
	}
%>

<form method="post" action="index">
	<div class="mainlogindiv">
		<div style="padding: 10px;">
			<input type="hidden" name="fkt" value="createevent" /> <input
				type="hidden" name="create" value="1" /> <input type="hidden"
				name="id" value="<% out.print(request.getAttribute("id")); %>" /> <input
				type="hidden" name="datum"
				value="<% out.print(request.getAttribute("date")); %>" /> An dieser
			Stelle können Sie ein Event anlegen, jenes anschließend in der
			Eventsuche aufscheint. Bitte geben Sie sämtliche Informationen
			an (alle Felder sind Pflichtfelder) und klicken sie abschließend auf
			"Absenden".<br /> Das Datum und die Location haben Sie bereits in
			den vorherigen Schritten ausgewählt. <br /> <br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Location:</div>
			<div style="width: 50%; float: right;">
				<span style="font-weight: bold;"><%  out.print(request.getAttribute("locationname")); %></span>
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Datum:</div>
			<div style="width: 50%; float: right;">
				<% out.print(request.getAttribute("date")); %>
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Eventname:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="name" size="50"
					style="text-align: left; width: 220px"
					value="<% if(request.getAttribute("name") != null) out.print(request.getAttribute("name")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Beginn/Uhrzeit (HH:MM):</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="uhrzeit" size="25"
					style="text-align: left; width: 220px"
					value="<% if(request.getAttribute("uhrzeit") != null) out.print(request.getAttribute("uhrzeit")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Maximale Teilnehmer:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="maxteilnehmer" size="25"
					style="text-align: right; width: 160px"
					value="<% if(request.getAttribute("maxteilnehmer") != null) out.print(request.getAttribute("maxteilnehmer")); %>" />
				Personen
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Preis:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="preis" size="25"
					style="text-align: right; width: 185px"
					value="<% if(request.getAttribute("preis") != null) out.print(request.getAttribute("preis")); %>" />
				EUR
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Kurzbeschreibung:</div>
			<div style="width: 50%; float: right;"></div>
			<div
				style="width: 100%; float: left; font-weight: bold; padding-bottom: 20px;">
				<textarea style="width: 490px; font-size: 13px;" name="besch"
					rows="5" cols="40"><% if(request.getAttribute("besch") != null) out.print(request.getAttribute("besch")); %></textarea>
			</div>

			<div style="text-align: right; padding: 5px;">
				<input class="button" type="submit" value="Absenden" />
			</div>
		</div>
	</div>
</form>


<%@ include file="tpl_footer.jsp"%>
