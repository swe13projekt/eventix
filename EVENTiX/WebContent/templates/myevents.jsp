<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="management.*"%>
<%@ page import="events.*"%>
<%@ page import="locations.*"%>
<%@ page import="users.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ include file="tpl_header.jsp"%>
<% 
	if(session.getAttribute("status") == null || (Integer) session.getAttribute("status") != 1){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}
%>

<div class="mheadline">Meine erstellten Events</div>

<%	
	UserManagement um = null;
	ArrayList<Event> myevents = null;
	int eventscount = 0;
	try{
		um = new UserManagement();	
		myevents = um.getMyEvents((String) session.getAttribute("username"));
		if(myevents != null){ eventscount = myevents.size(); }		
	}catch(Exception e){
		response.sendRedirect("index?fkt=error");
	}

%>

<span style="font-weight: bold;">Sie haben <%=eventscount %> bevorstehende Veranstaltung<% if(eventscount != 1){out.print("en");} %> erstellt:
</span>
<br />
<br />

<% 
	if(eventscount == 0){
%>
<span style="font-style: italic;">Sie haben keine bevorstehenden Veranstaltungen erstellt.</span>
<%		
	} else {
		for(int i=0; i < myevents.size(); i++){
			Event myevent = myevents.get(i);
			
			out.println("<a style=\"font-size: 14px;\" href=\"index?fkt=vieweventpage&id=" + myevent.getId() +"\">" +  myevent.getName() + "</a>");
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Datum: </span>");
			out.println(myevent.getDatumToString());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Uhrzeit: </span>");
			out.println(myevent.getBeginnzeit());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Location: </span>");
			out.println("<a style=\"font-weight: normal;\" href=\"index?fkt=viewlocationpage&id=" + myevent.getId() +"\">" +  myevent.getLocationToString() + "</a>");
			out.println("<br />");
			
			out.println("<a style=\"color: red;\" href=\"index?fkt=deleteevent&id=" + myevent.getId() +"\" onclick=\"return window.confirm('M�chten Sie dieses Event wirklich l�schen?');\">&raquo; EVENT L�SCHEN </a>");
			
			out.println("<br />");
			out.println("<br />");
		}
	}
%>



<%@ include file="tpl_footer.jsp"%>
