<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="management.*"%>
<%@ page import="events.*"%>
<%@ page import="locations.*"%>
<%@ page import="users.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ include file="tpl_header.jsp"%>

<% 
	if(session.getAttribute("status") == null){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}
	
	EventManagement em = new EventManagement();	
	Event myevent = null;
		try{
			myevent = em.getEvent((Integer) request.getAttribute("id"));  
		}catch(Exception e){ 
				response.sendRedirect("index?fkt=error&code=eventnotfound");
		}
	String eventname = myevent.getName();
	String beginnzeit = myevent.getBeginnzeit();
	String veranstalter = myevent.getVeranstalter();
	String veranstaltername = myevent.getVeranstalterName();
	String datum = myevent.getDatumToString();
	String beschreibung = myevent.getBeschreibung();
		beschreibung = beschreibung.replaceAll("\n", "<br />"); 
	String maxteilnehmer = String.valueOf(myevent.getMaxTeilnehmer());
	String teilnehmer = String.valueOf(myevent.getTeilnehmerAnzahl());
	String id = String.valueOf(myevent.getId());
	String location = myevent.getLocationToString();
	String adresse = myevent.getAdresse(); 
	String location_id = String.valueOf(myevent.getLocation_id());	
	String preis = String.valueOf(myevent.getPreis()); 
	boolean teilnahme = myevent.nimmtTeil((String) session.getAttribute("username"));
	boolean isveranstalter = myevent.isVeranstalter((String) session.getAttribute("username"));
%>

<div class="mheadline">
	<%=eventname %></div>

<span style="font-weight: bold">Datum:</span>
<%=datum %>
<br />
<span style="font-weight: bold">Uhrzeit:</span>
<%=beginnzeit %>
<br />
<span style="font-weight: bold">Preis:</span>
<% if(preis.equals("0.0")){ out.print("Gratis Eintritt");} else{ out.print(preis + " EUR");}  %>
<br />
<span style="font-weight: bold">Location:</span>
<a style="font-weight: normal;"
	href="index?fkt=viewlocationpage&id=<%=location_id %>"> <%=location %> </a>
<br />
<span style="font-weight: bold">Adresse:</span>
<%=adresse %>
<br />
<span style="font-weight: bold">Veranstalter:</span>
<%=veranstaltername %>
<br />
<span style="font-weight: bold">Maximale Teilnehmer:</span>
<%if(maxteilnehmer != "0") out.print(maxteilnehmer); %>
<br />
<span style="font-weight: bold">Angemeldete Teilnehmer:</span>
<%=teilnehmer %>
<br />

<span style="font-weight: bold">Beschreibung:</span>
<br />
<%=beschreibung %>
<br />
<br />
<%
	if(teilnahme){
%>
<span style="font-weight: bold; font-style: italic;">Sie sind f�r
	dieses Event angemeldet!</span>
<br />
<br />
<div style="text-align: left; padding: 5px;">
	<input
		onClick="location.href='index?fkt=eventteilnahme&kommt=nein&id=<%=id %>'"
		class="button" type="button" value="Vom Event Abmelden" />
</div>

<%	
	} else {
%>
<div style="text-align: left; padding: 5px;">
	<input
		onClick="location.href='index?fkt=eventteilnahme&kommt=ja&id=<%=id %>'"
		class="button" type="button" value="Am Event Teilnehmen" />
</div>
<%			
	}
%>
<%
	if(isveranstalter){
%>

<br />
<div style="text-align: left; padding: 5px; padding-top: 15px;">
	<input
		onClick="if(confirm('M�chten Sie dieses Event wirklich l�schen?')) location.href='index?fkt=deleteevent&id=<%=id %>'"
		style="background-color: #BE422C;" type="button"
		value="EVENT L�SCHEN" />
</div>

<br />
<br />
<span style="font-weight: bold;">Alle angemdeldeten Teilnehmer:</span>
<br />


<%	if(teilnehmer.equals("0")){
	out.print("<span style=\"font-style: italic;\">Es nimmt niemand an der Veranstaltung teil.</span>");
}
		ArrayList<RegUser> myteilnehmer = myevent.getTeilnehmer();
		for(int i=0; i < myteilnehmer.size(); i++){
			out.print(myteilnehmer.get(i).getFullName() + " (" + myteilnehmer.get(i).getUsername() + "), ");
		}
	}
%>





<%@ include file="tpl_footer.jsp"%>
