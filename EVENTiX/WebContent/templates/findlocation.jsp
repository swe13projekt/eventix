<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="management.*"%>
<%@ page import="events.*"%>
<%@ page import="locations.*"%>
<%@ page import="users.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ include file="tpl_header.jsp"%>
<% 
	if(session.getAttribute("status") == null || (Integer) session.getAttribute("status") != 1){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}
%>

<div class="mheadline">Event Anlegen - Locationsuche</div>

<% 
	if(request.getAttribute("error") != null){
%>
<div
	style="text-align: center; color: red; font-weight: bold; font-style: italic;"><%=request.getAttribute("error") %></div>
<%
	}
%>

<form method="get" action="index">
	<div class="mainlogindiv">
		<div style="padding: 10px;">
			<input type="hidden" name="fkt" value="findlocation" /> Bitte geben
			Sie das Datum an, f�r das Ihnen freie Locations angezeigt werden
			sollen.<br /> Die Angaben f�r die Locationbezeichnung und den
			Locationtypen sind optional. <br /> <br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">Datum
				(TT.MM.JJJJ):</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="datum" size="25" style="width: 220px"
					value="<% if(request.getAttribute("datum") != null) out.print(request.getAttribute("datum")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Locationbezeichnung:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="name" size="50" style="width: 220px"
					value="<% if(request.getAttribute("name") != null) out.print(request.getAttribute("name")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Locationtyp :</div>
			<div style="width: 50%; float: right;">
				<select style="width: 220px" name="typ" size="1">
					<option value=""
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("")) out.print(" selected=\"selected\""); %>>Alle
						Locations</option>
					<option value="Club"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Club")) out.print(" selected=\"selected\""); %>>Club</option>
					<option value="Partyraum"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Partyraum")) out.print(" selected=\"selected\""); %>>Partyraum</option>
					<option value="Konzertsaal"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Konzertsaal")) out.print(" selected=\"selected\""); %>>Konzertsaal</option>
					<option value="Konferenzraum"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Konferenzraum")) out.print(" selected=\"selected\""); %>>Konferenzraum</option>
					<option value="Lehrsaal"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Lehrsaal")) out.print(" selected=\"selected\""); %>>Lehrsaal</option>
				</select>
			</div>
			<br /> <br />

			<div style="text-align: right; padding: 5px;">
				<input class="button" type="submit" value="Suche" />
			</div>
		</div>
	</div>
</form>


<br />
<br />

<% 
	if(request.getAttribute("error") == null && request.getAttribute("locations") != null){
%>
<span style="font-weight: bold;">Verf�gbare Locations am <% out.print(request.getAttribute("datum")); %>:
</span>
<br /><br />

<% 
	if((Integer) request.getAttribute("locationcount") == 0){
%>
<span style="font-style: italic;">Es wurden keine Locations f�r
	Ihre Sucheingaben gefunden.</span>
<%		
	} else {
		ArrayList<Location> mylocations = null;
		
		try{
			mylocations = (ArrayList<Location>) request.getAttribute("locations");
		}
		catch(Exception e){
			response.sendRedirect("index?fkt=error");
		}
		
		for(int i=0; i < (Integer) request.getAttribute("locationcount"); i++){
			Location mylocation = mylocations.get(i);
			
			out.println("<a style=\"font-size: 14px;\" href=\"index?fkt=viewlocationpage&id=" + mylocation.getId() + "&rdatum=" + request.getAttribute("datum") + "\">" +  mylocation.getName() + " (Locationpage anzeigen) </a>");
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Adresse: </span>");
			out.println(mylocation.getAdresse());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Locationtyp: </span>");
			out.println(mylocation.getType());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Preis: </span>");
			out.println(mylocation.getPreis() + " EUR");
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Kapazit�t: </span>");
			out.println(mylocation.getMaxTeilnehmer() + " Teilnehmer");
			out.println("<br />");
			
			out.println("<a style=\"font-weight: normal;\" href=\"index?fkt=createevent&id=" + mylocation.getId() + "&date=" + request.getAttribute("datum") + "\">" + ">> Location f�r ein Event buchen" + "</a>");
			out.println("<br />");
			
			out.println("<br />");
		}
	}
%>

<% } %>



<%@ include file="tpl_footer.jsp"%>
