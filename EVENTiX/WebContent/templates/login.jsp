<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ include file="tpl_header.jsp"%>

<div class="mheadline">Login</div>


<form method="post" action="index">
	<div class="mainlogindiv">
		<div style="padding: 10px;">
			<input type="hidden" name="fkt" value="login" /> Bitte geben Sie
			Ihren Usernamen und Ihr Passwort ein und klicken Sie anschließend auf
			"Einloggen".<br /> Die Login-Session wird nur bis zum Schließen des
			Internet-Browsers gespeichert. <br /> <br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Username:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="user" size="20" style="width: 110px"
					value="" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Passwort:</div>
			<div style="width: 50%; float: right;">
				<input type="password" name="password" size="20"
					style="width: 110px" value="" />
			</div>
			<br /> <br />
			<div style="text-align: right; padding: 5px;">
				<input class="button" type="submit" value="Einloggen" />
			</div>
		</div>
	</div>
</form>


<%@ include file="tpl_footer.jsp"%>
