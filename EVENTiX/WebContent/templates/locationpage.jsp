<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="management.*"%>
<%@ page import="events.*"%>
<%@ page import="locations.*"%>
<%@ page import="users.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ include file="tpl_header.jsp"%>

<% 
	if(session.getAttribute("status") == null){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}
	
%>

<%	
	LocationManagement lm = new LocationManagement();	
	Location mylocation = null;
		try{
			mylocation = lm.getLocation((Integer) request.getAttribute("id"));
		}catch(Exception e){ 
				response.sendRedirect("index?fkt=error&code=locationnotfound");
		}
	String locationname = mylocation.getName();
	String besitzer = mylocation.getBesitzerToString(); 
	String typ = mylocation.getType();
	String id = String.valueOf(mylocation.getId());
	String preis = String.valueOf(mylocation.getPreis());
	String maxteilnehmer = String.valueOf(mylocation.getMaxTeilnehmer());
	String adresse = mylocation.getAdresse();
	String beschreibung = mylocation.getBeschreibung();
	beschreibung = beschreibung.replaceAll("\n", "<br />"); 
	
	boolean isvermieter = mylocation.isVermieter((String) session.getAttribute("username"));
	
%>

<div class="mheadline">
	Location:
	<%=locationname %></div>


<span style="font-weight: bold">Adresse:</span>
<%=adresse %>
<br />
<span style="font-weight: bold">Besitzer:</span>
<%=besitzer %>
<br />
<span style="font-weight: bold">Typ:</span>
<%=typ %>
<br />
<span style="font-weight: bold">Preis:</span>
<%=preis %> EUR
<br />
<span style="font-weight: bold">Kapazit�t:</span>
<%=maxteilnehmer %> Personen
<br />
<span style="font-weight: bold">Beschreibung:</span>
<br /><%=beschreibung %>
<br />

<%
	if(request.getAttribute("rdatum") != null){  
%>
<div style="text-align: left; padding: 5px; padding-top: 15px;">
	<input
		onClick="location.href='index?fkt=createevent&id=<%=id %>&date=<% out.print(request.getAttribute("rdatum")); %>'"
		class="button" type="button"
		value="Location am <% out.print(request.getAttribute("rdatum")); %> buchen" />
</div>
<%
	}
%>


<%
	if(isvermieter){
%>
<br /><br />
<div style="text-align: left; padding: 5px; padding-top: 15px;">
	<input
		onClick="if(confirm('M�chten Sie diese Location wirklich l�schen?')) location.href='index?fkt=deletelocation&id=<%=id %>'"
		style="background-color: #BE422C;" type="button"
		value="LOCATION L�SCHEN" />
</div>
<%		
	}
%>


<%@ include file="tpl_footer.jsp"%>
