<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ include file="tpl_header.jsp"%>

<div class="mheadline">Sign Up</div>
<% 
	if(request.getAttribute("error") != null){
%>
<div
	style="text-align: center; color: red; font-weight: bold; font-style: italic;"><%=request.getAttribute("error") %></div>
<%
	}
%>

<form method="post" action="index">
	<div class="mainlogindiv">
		<div style="padding: 10px;">
			<input type="hidden" name="fkt" value="signup" /> Um das Service von
			EVENTiX n�tzen zu k�nnen, m�ssen Sie sich registrieren.<br /> Bitte
			geben Sie s�mtliche Informationen an (alle Felder sind Pflichtfelder)
			und klicken sie abschlie�end auf "Absenden". <br /> <br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Username:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="username" size="25" style="width: 170px"
					value="<% if(request.getAttribute("username") != null) out.print(request.getAttribute("username")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Passwort (mind. 6 Zeichen):</div>
			<div style="width: 50%; float: right;">
				<input type="password" name="password" size="25"
					style="width: 170px" value="" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Passwort (Wiederholen):</div>
			<div style="width: 50%; float: right;">
				<input type="password" name="pwcheck" size="25" style="width: 170px"
					value="" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Email-Adresse:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="email" size="25" style="width: 170px"
					value="<% if(request.getAttribute("email") != null) out.print(request.getAttribute("email")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Vorname:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="vorname" size="25" style="width: 170px"
					value="<% if(request.getAttribute("vorname") != null) out.print(request.getAttribute("vorname")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Nachname:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="nachname" size="25" style="width: 170px"
					value="<% if(request.getAttribute("nachname") != null) out.print(request.getAttribute("nachname")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Sonderrolle:</div>
			<div style="width: 50%; float: right;">
				<select style="width: 170px" name="rolle" size="1">
					<option value="0"
						<% if(request.getAttribute("rolle") != null && request.getAttribute("rolle").equals("0")) out.print(" selected=\"selected\""); %>>(Keine)</option>
					<option value="1"
						<% if(request.getAttribute("rolle") != null && request.getAttribute("rolle").equals("1")) out.print(" selected=\"selected\""); %>>Veranstalter</option>
					<option value="2"
						<% if(request.getAttribute("rolle") != null && request.getAttribute("rolle").equals("2")) out.print(" selected=\"selected\""); %>>Raumvermieter</option>
					<option value="3"
						<% if(request.getAttribute("rolle") != null && request.getAttribute("rolle").equals("3")) out.print(" selected=\"selected\""); %>>Analyst</option>
				</select>
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Geschlecht:</div>
			<div style="width: 50%; float: right;">
				<select style="width: 170px" name="gender" size="1">
					<option value="m"
						<% if(request.getAttribute("gender") != null && request.getAttribute("gender").equals("m")) out.print(" selected=\"selected\""); %>>M�nnlich</option>
					<option value="w"
						<% if(request.getAttribute("gender") != null && request.getAttribute("gender").equals("w")) out.print(" selected=\"selected\""); %>>Weiblich</option>
				</select>
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Geburtsdatum (TT.MM.JJJJ):</div>
			<div style="width: 50%; float: right;">
				<select style="width: 40px" name="day" size="1">
					<% 
					    for(int i=1; i < 32; i++){ out.println("<option value=\"" + i +"\">" + i + "</option>"); }
					  %>
				</select> . <select style="width: 40px" name="month" size="1">
					<% 
					    for(int i=1; i < 13; i++){ out.println("<option value=\"" + i +"\">" + i + "</option>"); }
					  %>
				</select> . <select style="width: 65px" name="year" size="1">
					<% 
					    for(int i=2015; i > 1899; i--){ out.println("<option value=\"" + i +"\">" + i + "</option>"); }
					  %>
				</select>

			</div>
			<br />
			<% if(request.getAttribute("day") != null) out.print("<span style=\"font-style: italic; color: red;\">Bitte neu angeben!</span>"); %>
			<br />
			<div style="text-align: right; padding: 5px;">
				<input class="button" type="submit" value="Absenden" />
			</div>
		</div>
	</div>
</form>


<%@ include file="tpl_footer.jsp"%>
