<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="management.*"%>
<%@ page import="events.*"%>
<%@ page import="locations.*"%>
<%@ page import="users.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ include file="tpl_header.jsp"%>
<% 
	if(session.getAttribute("status") == null){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}
	
%>

<div class="mheadline">Mein Kalender</div>
<%	
	UserManagement um = null;
	ArrayList<Event> myevents = null;
	int eventscount = 0;
	try{
		um = new UserManagement();	
		myevents = um.getMyCalender((String) session.getAttribute("username"));
		if(myevents != null){ eventscount = myevents.size(); }		
	}catch(Exception e){
		response.sendRedirect("index?fkt=error&code=mycalender");
	}

%>
<% 
	if(eventscount != 0){
%>
<span style="font-weight: bold;"><%=eventscount %> bevorstehende Veranstaltung<% if(eventscount != 1){out.print("en");} %>:
</span>
<br />
<br />

<% 
	}
%>

<% 
	if(eventscount == 0){
%>
<span style="font-style: italic;">Es sind keine zukünftigen Events vorhanden, an denen Sie teilenhmen.</span>
<%		
	} else {
		for(int i=0; i < myevents.size(); i++){
			Event myevent = myevents.get(i);
			
			out.println("<a style=\"font-size: 14px;\" href=\"index?fkt=vieweventpage&id=" + myevent.getId() +"\">" +  myevent.getName() + "</a>");
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Datum: </span>");
			out.println(myevent.getDatumToString());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Uhrzeit: </span>");
			out.println(myevent.getBeginnzeit());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Location: </span>");
			out.println("<a style=\"font-weight: normal;\" href=\"index?fkt=viewlocationpage&id=" + myevent.getLocation_id() +"\">" +  myevent.getLocationToString() + "</a>");
			out.println("<br />");
			
			out.println("<br />");
		}
	}
%>





<%@ include file="tpl_footer.jsp"%>
