<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ include file="tpl_header.jsp"%>

<div class="mheadline">Home</div>

EVENTiX erm�glicht es Ihnen, Ihre Veranstaltungen effizient zu
verwalten.
<br />
Um das Service n�tzen zu k�nnen, ist eine Anmeldung und ein Login n�tig.
<br />
<br />

Weitere Features:
<br />
Veranstalter k�nnen EVENTiX dazu n�tzen, um Ihre Events zu promoten.
<br />
Als Raumvermieter besteht die M�glichkeit, �ber EVENTiX Ihre
R�umlichkeiten f�r Veranstaltungen zur Verf�gung zu Stellen.

<%@ include file="tpl_footer.jsp"%>
