<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>EVENTiX</title>

<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="content-language" content="de" />
<meta name="author" content="EVENTiX Team" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="Abstract" content="" />
<link href="www/default.css" rel="stylesheet" type="text/css"
	media="all" />
<link rel="shortcut icon" href="www/favicon.gif" type="image/x-icon" />
</head>

<body>
	<div id="main">
		<div id="header">&nbsp;</div>
		<div id="navi">
			<ul>
				<% if(session.getAttribute("username") == null){ %>
				<li><a href="index?page=home">Home</a></li>
				<li><a href="index?page=login">Login</a></li>
				<li><a href="index?page=signup">Sign Up</a></li>
				<% } %>

				<% if(session.getAttribute("username") != null){ %>
				<li><a href="index?page=mycalender">Mein Kalender</a></li>
				<li><a href="index?page=eventsearch">Eventsuche</a></li>
				<% } %>

				<% if(session.getAttribute("status") != null && (Integer) session.getAttribute("status") == 1){ %>
				<li><a href="index?page=myevents">Meine Events</a></li>
				<li><a href="index?page=findlocation">Event anlegen</a></li>
				<% } %>

				<% if(session.getAttribute("status") != null && (Integer) session.getAttribute("status") == 2){ %>
				<li><a href="index?page=mylocations">Meine Locations</a></li>
				<li><a href="index?page=createlocation">Location Anlegen</a></li>
				<% } %>

				<% if(session.getAttribute("status") != null && (Integer) session.getAttribute("status") == 3){ %>
				<li><a href="index?page=statistics">Statisken</a></li>
				<% } %>

				<% if(session.getAttribute("username") != null){ %>
				<li><a href="index?fkt=logout">Logout</a></li>
				<% } %>
			</ul>
		</div>
		<div id="textbox">
			<div id="textbtm">
				<div id="text">