<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ include file="tpl_header.jsp"%>

<% 
	if(session.getAttribute("status") == null || (Integer) session.getAttribute("status") != 2){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}
%>

<div class="mheadline">Location Anlegen</div>
<% 
	if(request.getAttribute("error") != null){
%>
<div
	style="text-align: center; color: red; font-weight: bold; font-style: italic;"><%=request.getAttribute("error") %></div>
<%
	}
%>

<form method="post" action="index">
	<div class="mainlogindiv">
		<div style="padding: 10px;">
			<input type="hidden" name="fkt" value="createlocation" /> An dieser
			Stelle können Sie eine Location anlegen, jene anschließend von
			Veranstaltern verwendet werden kann.<br /> Bitte geben Sie sämtliche
			Informationen an (alle Felder sind Pflichtfelder) und klicken sie
			abschließend auf "Absenden". <br /> <br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Locationbezeichnung:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="name" size="25" style="width: 220px"
					value="<% if(request.getAttribute("name") != null) out.print(request.getAttribute("name")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Adresse (Str., Plz, Ort):</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="adresse" size="50" style="width: 220px"
					value="<% if(request.getAttribute("adresse") != null) out.print(request.getAttribute("adresse")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">Preis
				(pro Tag):</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="preis" size="25"
					style="text-align: right; width: 185px"
					value="<% if(request.getAttribute("preis") != null) out.print(request.getAttribute("preis")); %>" />
				EUR
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Kapazität:</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="maxt" size="25"
					style="text-align: right; width: 160px"
					value="<% if(request.getAttribute("maxt") != null) out.print(request.getAttribute("maxt")); %>" />
				Personen
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Locationtyp :</div>
			<div style="width: 50%; float: right;">
				<select style="width: 220px" name="typ" size="1">
					<option value="Sonstiges"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Sonstiges")) out.print(" selected=\"selected\""); %>>Sonstiges</option>
					<option value="Club"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Club")) out.print(" selected=\"selected\""); %>>Club</option>
					<option value="Partyraum"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Partyraum")) out.print(" selected=\"selected\""); %>>Partyraum</option>
					<option value="Konzertsaal"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Konzertsaal")) out.print(" selected=\"selected\""); %>>Konzertsaal</option>
					<option value="Konferenzraum"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Konferenzraum")) out.print(" selected=\"selected\""); %>>Konferenzraum</option>
					<option value="Lehrsaal"
						<% if(request.getAttribute("typ") != null && request.getAttribute("typ").equals("Lehrsaal")) out.print(" selected=\"selected\""); %>>Lehrsaal</option>
				</select>
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Kurzbeschreibung:</div>
			<div style="width: 50%; float: right;"></div>
			<div
				style="width: 100%; float: left; font-weight: bold; padding-bottom: 20px;">
				<textarea style="width: 490px; font-size: 13px;" name="besch"
					rows="5" cols="40"><% if(request.getAttribute("besch") != null) out.print(request.getAttribute("besch")); %></textarea>
			</div>

			<div style="text-align: right; padding: 5px;">
				<input class="button" type="submit" value="Absenden" />
			</div>
		</div>
	</div>
</form>


<%@ include file="tpl_footer.jsp"%>
