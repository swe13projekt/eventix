<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="management.*"%>
<%@ page import="events.*"%>
<%@ page import="locations.*"%>
<%@ page import="users.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ include file="tpl_header.jsp"%>
<% 
	if(session.getAttribute("status") == null || (Integer) session.getAttribute("status") != 2){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}
%>

<div class="mheadline">Meine erstellten Locations</div>

<%	
	UserManagement um = null;
	ArrayList<Location> mylocations = null;
	int locationcount = 0;
	try{
		um = new UserManagement();	
		mylocations = um.getMyLocations((String) session.getAttribute("username"));
		if(mylocations != null){ locationcount = mylocations.size(); }		
	}catch(Exception e){
		response.sendRedirect("index?fkt=error");
	}

%>

<span style="font-weight: bold;">Sie haben <%=locationcount %> Location<% if(locationcount != 1){out.print("s");} %> erstellt:
</span>
<br />
<br />

<% 
	if(locationcount == 0){
%>
<span style="font-style: italic;">Sie haben keine Events erstellt.</span>
<%		
	} else {
		for(int i=0; i < mylocations.size(); i++){
			Location mylocation = mylocations.get(i);
			
			out.println("<a style=\"font-size: 14px;\" href=\"index?fkt=viewlocationpage&id=" + mylocation.getId() + "\">" +  mylocation.getName() + " (Locationpage anzeigen) </a>");
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Adresse: </span>");
			out.println(mylocation.getAdresse());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Locationtyp: </span>");
			out.println(mylocation.getType());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Preis: </span>");
			out.println(mylocation.getPreis() + " EUR");
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Kapazit�t: </span>");
			out.println(mylocation.getMaxTeilnehmer() + " Teilnehmer");
			out.println("<br />");
			
			out.println("<a style=\"color: red;\" href=\"index?fkt=deletelocation&id=" + mylocation.getId() +"\" onclick=\"return window.confirm('M�chten Sie diese Location wirklich l�schen?');\">&raquo; LOCATION L�SCHEN </a>");
			out.println("<br />");
			
			out.println("<br />");
		}
	}
%>



<%@ include file="tpl_footer.jsp"%>
