<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="management.*"%>
<%@ page import="events.*"%>
<%@ page import="locations.*"%>
<%@ page import="users.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ include file="tpl_header.jsp"%>
<% 
	if(session.getAttribute("status") == null){ 
		response.sendRedirect("index?fkt=error&code=nopermission");
	}
	
%>

<%	
	EventManagement em = null;
	ArrayList<Event> myevents = null;
	String name = "";
	String datum = "";
		if(request.getAttribute("datum") != null) datum = (String) request.getAttribute("datum");
		if(request.getAttribute("name") != null) name = (String) request.getAttribute("name");
	int eventscount = 0;
	try{
		em = new EventManagement();	
		myevents = em.findEvents(name, datum);
		eventscount = myevents.size();		
	}catch(Exception e){
		response.sendRedirect("index?fkt=error&code=findeventerror");
	}

%>

<div class="mheadline">Eventsuche</div>

<form method="get" action="index">
	<div class="mainlogindiv">
		<div style="padding: 10px;">
			<input type="hidden" name="fkt" value="eventsearch" /> 
			Bitte geben Sie einen Namen und ein Datum ein, auf das Sie Ihre Eventsuche einschränken möchten.
			Beide Felder sind optional. Wenn Sie beide Felder freilassen, werden ihnen alle Events angezeigt, die in der Zukunft geplant sind.
			
			 <br /> <br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">Eventname:
				</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="name" size="50" style="width: 220px"
					value="<% if(request.getAttribute("name") != null) out.print(request.getAttribute("name")); %>" />
			</div>
			<br /> <br />
			<div style="width: 50%; float: left; font-weight: bold;">
				Datum (TT.MM.JJJJ):</div>
			<div style="width: 50%; float: right;">
				<input type="text" name="datum" size="25" style="width: 220px"
					value="<% if(request.getAttribute("datum") != null) out.print(request.getAttribute("datum")); %>" />
			</div>
			<br /> <br />
			<div style="text-align: right; padding: 5px;">
				<input class="button" type="submit" value="Suche" />
			</div>
		</div>
	</div>
</form>

<br />

<span style="font-weight: bold;">Gefundene Events (<%=eventscount %>):
</span>
<br />
<br />

<% 
	if(eventscount == 0){
%>
<span style="font-style: italic;">Es wurden keine Eventsgefunden, die auf Ihre Sucheingaben passen.</span>
<%		
	} else {
		for(int i=0; i < myevents.size(); i++){
			Event myevent = myevents.get(i);
			
			out.println("<a style=\"font-size: 14px;\" href=\"index?fkt=vieweventpage&id=" + myevent.getId() +"\">" +  myevent.getName() + "</a>");
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Datum: </span>");
			out.println(myevent.getDatumToString());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Uhrzeit: </span>");
			out.println(myevent.getBeginnzeit());
			out.println("<br />");
			
			out.println("<span style=\"font-weight: bold;\">Location: </span>");
			out.println("<a style=\"font-weight: normal;\" href=\"index?fkt=viewlocationpage&id=" + myevent.getLocation_id() +"\">" +  myevent.getLocationToString() + "</a>");
			out.println("<br />");
			
			out.println("<br />");
		}
	}
%>





<%@ include file="tpl_footer.jsp"%>
