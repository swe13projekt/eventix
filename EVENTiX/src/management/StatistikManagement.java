package management;

import java.io.IOException;
import java.util.ArrayList;

import locations.Location;
import users.RegUser;
import dao.EventManagementDAO;
import dao.LocationManagementDAO;
import dao.UserManagementDAO;
import events.Event;
/**
 * 
 * @author Dominic Boehm
 * @author James Patrick Salazar
 *
 */
public class StatistikManagement {
	
	
	public int gibAnzRegUser() throws IOException {
		UserManagementDAO umdao = new UserManagementDAO();
		ArrayList<RegUser> al_regu = umdao.getUserList();
		return al_regu.size();
	}
	
	public int gibAnzVeranstalter() throws IOException {
		
		UserManagementDAO umdao = new UserManagementDAO();
		ArrayList<RegUser> al_regu = umdao.getUserList();
		int anz_vera = 0;
		
		for(int i = 0; i < al_regu.size(); i++){
			if(al_regu.get(i).getUserStatus() == 1)
				anz_vera ++;
		}
		return anz_vera;
	}
	
	public int gibAnzVermieter() throws IOException{

		
		UserManagementDAO umdao = new UserManagementDAO();
		ArrayList<RegUser> al_regu = umdao.getUserList();
		int anz_verm = 0;
		
		for(int i = 0; i < al_regu.size(); i++){
			if(al_regu.get(i).getUserStatus() == 2)
				anz_verm ++;
		}
		return anz_verm;
	}
	
	public int gibAnzAnalyst() throws IOException{
		
		UserManagementDAO umdao = new UserManagementDAO();
		ArrayList<RegUser> al_regu = umdao.getUserList();
		int anz_ana = 0;
		
		for(int i = 0; i < al_regu.size(); i++){
			if(al_regu.get(i).getUserStatus() == 3)
				anz_ana ++;
		}
		return anz_ana;
	}
	
	public int gibAnzNormale() throws IOException{
		
		return gibAnzRegUser() - (gibAnzAnalyst() + gibAnzVeranstalter() + gibAnzVermieter());
	}
	
	public int gibAnzEvent() throws IOException{

		EventManagementDAO emdao = new EventManagementDAO();
		ArrayList<Event> al_event = emdao.getEventList();
		return al_event.size();
	}
	
	public int gibAnzLocation() throws IOException{

		
		LocationManagementDAO lmdao = new LocationManagementDAO();
		ArrayList<Location> al_loc = lmdao.getLocationList();
		return al_loc.size();
	}
	
	public int gibDurchschnittTeilnehmerProEvent() throws IOException{
		
		EventManagementDAO emdao = new EventManagementDAO();
		ArrayList<Event> al_event = emdao.getEventList();
		UserManagementDAO umdao = new UserManagementDAO();
		ArrayList<RegUser> al_regu = umdao.getUserList();
		int teiln_allevent = 0;
		
		for(int i = 0; i < al_event.size(); i++){
			for(int ia = 0; ia < al_regu.size(); ia++){
				if(al_event.get(i).nimmtTeil(al_regu.get(ia).getUsername()) == true)
					teiln_allevent ++;
			}
		}
		
		return teiln_allevent / al_event.size();
	}
	
	public int gibDurchschnittEventProLocation() throws IOException{

		
		return gibAnzEvent() / gibAnzLocation();
	}
	
	public double gibDurchschnittPreisProLocation() throws IOException{

		
		LocationManagementDAO lmdao = new LocationManagementDAO();
		ArrayList<Location> al_loc = lmdao.getLocationList();
		double preis_allloc = 0;
		
		for(int i =0;  i < al_loc.size(); i++){
			preis_allloc += al_loc.get(i).getPreis();
		}
		return (double) (Math.round(preis_allloc / al_loc.size() * 100)) / 100;
	}
	
	public double gibDurchschnittPreisProEvent() throws IOException{

		EventManagementDAO emdao = new EventManagementDAO();
		ArrayList<Event> al_event = emdao.getEventList();
		double preis_allevents = 0;
		
		for(int i =0;  i < al_event.size(); i++){
			preis_allevents += al_event.get(i).getPreis();
		}
		return (double) (Math.round(preis_allevents / al_event.size() * 100)) / 100;
	}
	
	public double gibDurchschnittKapaProLocation() throws IOException{

		
		EventManagementDAO emdao = new EventManagementDAO();
		ArrayList<Event> al_event = emdao.getEventList();
		int kapa_allloc = 0;
		
		for(int i =0;  i < al_event.size(); i++){
			kapa_allloc += al_event.get(i).getMaxTeilnehmer();
		}
		return kapa_allloc / al_event.size();
	}
	public double gibProzentWeiblich() throws IOException {
		
		UserManagementDAO umdao = new UserManagementDAO();
		ArrayList<RegUser> al_regu = umdao.getUserList();
		double w = 0;
		
		for(int i = 0; i < al_regu.size(); i++){
			if(al_regu.get(i).getGeschlecht() == 'w')
				w += 1;
		}
		
		return (double) (Math.round(100*(w / al_regu.size()) * 100)) / 100;
	}
	
	public double gibProzentMannlich() throws IOException {
		
		UserManagementDAO umdao = new UserManagementDAO();
		ArrayList<RegUser> al_regu = umdao.getUserList();
		double m = 0;
		
		for(int i = 0; i < al_regu.size(); i++){
			if(al_regu.get(i).getGeschlecht() == 'm')
				m += 1;
		}
		
		return (double) (Math.round(100*(m / al_regu.size()) * 100)) / 100;
	}
	public double gibProzentVeranstalter() throws IOException{

		double a = gibAnzVeranstalter();
		double b = gibAnzRegUser();
		double c = 100*(a/b);
		return (double) (Math.round(c * 100)) / 100;
	}
	
	public double gibProzentVermieter() throws IOException{
		double a = gibAnzVermieter();
		double b = gibAnzRegUser();
		double c = 100*(a/b);
		
		return (double) (Math.round(c * 100)) / 100;
	}
	
	public double gibProzentAnalyst() throws IOException{

		double a = gibAnzAnalyst();
		double b = gibAnzRegUser();
		double c = 100*(a/b);
		
		return (double) (Math.round(c * 100)) / 100;
	}
	
	public double gibProzentNormaler() throws IOException{
		
		double a = gibAnzNormale();
		double b = gibAnzRegUser();
		double c = 100*(a/b);
		
		return (double) (Math.round(c * 100)) / 100;
	}
	
}
