package management;

import users.RegUser;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import dao.EventManagementDAO;
import dao.UserManagementDAO;
import events.Event;

/**
 * <h2>Eventmanagement
 * 
 * @author Dominic Böhm
 * @version 1.0 Klasse zum verwalten der Klasse {@link Event}. Bietet sozusagen
 *          die Funktionen damit Daten, welche aus dem
 *          {@link EventManagementDAO} dem {@link Frontend}zur Verfügung
 *          gestellt werden können
 * 
 */
public class EventManagement {

	private EventManagementDAO edau;

	/**
	 * Konstruktor
	 * @throws IOException 
	 */
	public EventManagement() throws IOException {
		edau = new EventManagementDAO();
	}

	/**
	 * Die Methode createEvent stellt die Funktion zum Erstellen eines Events
	 * zur Verfügung
	 * 
	 * @param name
	 *            Name des Events
	 * @param location_id
	 *            Ort an dem das Event stattfindet
	 * @param datum
	 *            Tag an dem das Event stattfindet
	 * @param maxTeilnehmer
	 *            Die maximale Anzahl der Teilnehmer
	 * @param preis
	 *            Eintrittspreis
	 * @param beschreibung
	 *            Kurzbeschreibung des Events
	 * @param beginnzeit
	 *            Die Startzeit
	 * @param veranstalter
	 *            ID der Person, die das Event anlegt bzw veranstaltet
	 * @throws IOException 
	 */
	public void createEvent(String name, int location_id, String datum,
			String maxTeilnehmer, String preis, String beschreibung,
			String beginnzeit, String veranstalter) throws IOException {
		
		SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
		int id = 0;
		if(edau.getEventList().size()!= 0)
		{
			id = edau.getEventList().get(edau.getEventList().size() - 1).getId() + 1;
		}
		
		if(!(datum.equals(""))){
			Date pdatum = null;
			try {
				pdatum = sd.parse(datum);
				datum = sd.format(pdatum);
			}
			catch (Exception e) 
			{}
		}

		ArrayList<Event> arr_event = getAllEvents();

			for(int ec = 0; ec < arr_event.size(); ec++){
				if(location_id == arr_event.get(ec).getLocation_id()){
					if(arr_event.get(ec).getDatumToString().equals(datum)){
						
						throw new IllegalArgumentException("Die Location ist zum angegebenen Termin nicht frei!");
					}
				}
			}			
		
		double p = 0;
		int maxTeilnehmerint = 0;
		try{
			p = Double.valueOf(preis);
			maxTeilnehmerint = Integer.valueOf(maxTeilnehmer);
		}catch(Exception e){
			throw new IllegalArgumentException("Bitte geben Sie einen gültigen Preis und Teilnehmeranzahl an!");
		}
		

		Event e;
		ArrayList<RegUser> teilnehmer = new ArrayList<RegUser>(); 
		
		try {
			e = new Event(id, name, location_id, sd.parse(datum),
					maxTeilnehmerint, p, beschreibung, beginnzeit, teilnehmer,
					veranstalter);
		} catch (ParseException e1) {

			throw new IllegalArgumentException(
					"Bitte geben Sie das Datum im Format TT.MM.JJJJ ein.");
		}
				
		edau.addEvent(e);
		
		try{
			anmeldenZuEvent(veranstalter, String.valueOf(id));
		}catch(Exception e1){
			throw new IllegalArgumentException("Location wurde erstellt. Allerdings konnten Sie nicht zum Event angemeldet werden.");
		}

	}

	/**
	 * Die Methode editEvent stellt die Funktion zum späteren Editieren des
	 * Events zur Verfügung
	 * 
	 * @param id
	 *            Identifikation des Events
	 * @param name
	 *            Name des Events
	 * @param location_id
	 *            Ort an dem das Event stattfindet
	 * @param datum
	 *            Tag an dem das Event stattfindet
	 * @param maxTeilnehmer
	 *            Die maximale Anzahl der Teilnehmer
	 * @param preis
	 *            Eintrittspreis
	 * @param beschreibung
	 *            Kurzbeschreibung des Events
	 * @param beginnzeit
	 *            Die Startzeit
	 * @param teilnehmer
	 *            Liste aller Teilnehmer
	 * @param veranstalter
	 *            ID der Person, die das Event anlegt bzw veranstaltet
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void editEvent(int id, String name, int location_id, Date datum,
			int maxTeilnehmer, double preis, String beschreibung,
			String beginnzeit, String veranstalter) throws IllegalArgumentException, IOException {

		Event e = edau.getEventbyID(id);
		if (!(name.isEmpty())) {
			e.setName(name);
		}
		if (maxTeilnehmer != 0) {
			e.setMaxTeilnehmer(maxTeilnehmer);
		}
		if (datum != null) {
			e.setDatum(datum);
		}
		if (!(beschreibung.isEmpty())) {
			e.setBeschreibung(beschreibung);
		}
		if (preis != 0.0) {
			e.setPreis(preis);
		}
		if (!(beginnzeit.isEmpty())) {
			e.setBeginnzeit(beginnzeit);
		}
		edau.updateEvent(e);

	}

	/**
	 * Löscht ein vorhandenes Event
	 * 
	 * @param id
	 *            Id des zulöschenden Events
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void deleteEvent(String id) throws IllegalArgumentException, IOException {
		edau.deleteEvent(id);

	}

	/**
	 * Ein User kann sich mit seinem Username zu einem Event anmelden
	 * 
	 * @param username
	 * @param id
	 *            Identifikation des Events
	 * @throws IOException 
	 */
	public void anmeldenZuEvent(String username, String id) throws IOException {

		ArrayList<Event> arr_events = getAllEvents();
		UserManagementDAO udau = new UserManagementDAO();

		for (Event e : arr_events) {
			if ((e.getId() + "").equals(id)) {
				ArrayList<RegUser> myteilnehmer = e.getTeilnehmer();
				for(int i=0; i < myteilnehmer.size(); i++){
					if(myteilnehmer.get(i).getUsername().equals(username)){
						throw new IllegalArgumentException("Sie sind bereits bei diesem Event angemeldet!");
					}
				}
				
					e.getTeilnehmer().add(udau.getUserbyUsername(username));
					edau.updateEvent(e);
	
					return;
			}
		}
		throw new IllegalArgumentException("Sie konnten nicht zum Event angemeldet werden.");

	}

	/**
	 * User kann sich von einem Event abmelden
	 * 
	 * @param username
	 * @param id
	 *            Identifikaion des Events
	 * @throws IOException 
	 */
	public void abmeldenvonEvent(String username, String id) throws IOException {

		ArrayList<Event> arr_events = getAllEvents();
		

		for (Event e : arr_events) {
			if ((e.getId() + "").equals(id)) {
				ArrayList<RegUser> myteilnehmer = e.getTeilnehmer();
				for(int i=0; i < myteilnehmer.size(); i++){
					if(myteilnehmer.get(i).getUsername().equals(username)){
						myteilnehmer.remove(myteilnehmer.get(i));
						e.setTeilnehmer(myteilnehmer);
						edau.updateEvent(e);
		
						return;
					}
				}
			}
		}
		throw new IllegalArgumentException("Sie waren nie zu diesem Event angemeldet!");
	}

	/**
	 * Gibt alle Events an einem bestimmten Datum zurück
	 * 
	 * @param name
	 * @param datum
	 * @return Liste aller möglichen Events
	 * @throws IOException 
	 */
	public ArrayList<Event> findEvents(String name, String datum) throws IOException {
		ArrayList<Event> arr_oldevents = getAllEvents();
		ArrayList<Event> arr_events = new ArrayList<Event>();
		ArrayList<Event> arr_foundevents = new ArrayList<Event>();
		
		Date today = new Date();
		
		if(!(datum.isEmpty())){
			SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
			Date pdatum = null;
			try {
				pdatum = sd.parse(datum);
				datum = sd.format(pdatum);
			}
			catch (Exception e) 
			{
				// No Exception bei Eventsearch -> einfach keine Resultate
			}
		}
		
		for(int a = 0; a < arr_oldevents.size(); a++){
			if(!(arr_oldevents.get(a).getDatum().before(today))){
				arr_events.add((arr_oldevents.get(a)));
			}
		}
		
		if(name.isEmpty() && datum.isEmpty()){
			arr_foundevents = arr_events;
		}
		else if(name.isEmpty()){
			for(int i = 0; i < arr_events.size(); i++){
				if(arr_events.get(i).getDatumToString().equals(datum)){
					arr_foundevents.add((arr_events.get(i)));
				}
				
			}
		}
		else if(datum.isEmpty()){
			for(int i = 0; i < arr_events.size(); i++){
				if(arr_events.get(i).getName().toLowerCase().contains(name.toLowerCase())){
					arr_foundevents.add((arr_events.get(i)));
				}	
			}
		}
		else { 
			for(int i = 0; i < arr_events.size(); i++){
				if(arr_events.get(i).getDatumToString().equals(datum) && arr_events.get(i).getName().toLowerCase().contains(name.toLowerCase())){
					arr_foundevents.add((arr_events.get(i)));
				}
			}
		}
			
			
		Collections.sort(arr_foundevents);
		return arr_foundevents;

	}

	/**
	 * gibt ein spezielles Event zurück
	 * 
	 * @param id
	 * @return Event
	 * @throws IOException 
	 */
	public Event getEvent(int id) throws IOException {
		ArrayList<Event> arr_events = getAllEvents();
		for (Event e : arr_events) {
			if (e.getId() == id) {
				return e;
			}
		}

		throw new ArrayStoreException("Event wurde nicht gefunden.");

	}

	/**
	 * liefert alle Events
	 * 
	 * @return Liste aller Events
	 * @throws IOException 
	 */
	public ArrayList<Event> getAllEvents() throws IOException {
		return edau.getEventList();
	}

}
