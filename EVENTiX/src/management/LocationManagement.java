/**
 * 
 */
package management;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import dao.LocationManagementDAO;
import dao.UserManagementDAO;
import events.Event;
import locations.Location;

/**
 * <h2>LocationManagement 
 * 
 * @author Dominic Böhm
 * @version 1.0 Klasse zum verwalten der Klasse {@link Location}. Bietet
 *          sozusagen die Funktionen damit Daten, welche aus dem
 *          {@link LocationManagementDAO} dem {@link Frontend}zur Verfügung
 *          gestellt werden können
 * 
 */
public class LocationManagement {

	private LocationManagementDAO ldao;

	/**
	 * Konstruktor
	 * @throws IOException 
	 */
	public LocationManagement() throws IOException {
		ldao = new LocationManagementDAO();
	}

	/**
	 * Die Methode createLocation bietet die Funktion eine Location zu erzeugen.
	 * 
	 * @param name
	 * @param maxTeilnehmer
	 * @param type
	 * @param beschreibung
	 * @param preis
	 * @param adresse
	 * @param username
	 * @throws IOException 
	 */
	public void createLocation(String name, int maxTeilnehmer, String type,
			String beschreibung, double preis, String adresse, String username) throws IOException {

		UserManagementDAO udao = new UserManagementDAO();
		Location l = null;
		int id = 0;
		if (udao.getUserbyUsername(username) != null) {
			if(ldao.getLocationList().size() == 0)
			{
				 l = new Location(id, name, maxTeilnehmer, type,
						beschreibung, preis, adresse, username);
			}
			else
			{
				id = ldao.getLocationList().get(ldao.getLocationList().size()-1).getId() + 1; 
				 l = new Location(id, name, maxTeilnehmer, type,
						beschreibung, preis, adresse, username);
			}
			ldao.addLocation(l);
		} else {
			throw new IllegalArgumentException("Der User konnte nicht gefunden werden!");
		}

	}

	/**
	 * Methode zum späteren Editieren einer Location
	 * 
	 * @param id
	 * @param name
	 * @param maxTeilnehmer
	 * @param type
	 * @param beschreibung
	 * @param preis
	 * @param adresse
	 * @param username
	 * @throws IOException 
	 */
	public void editLocation(int id, String name, int maxTeilnehmer,
			String type, String beschreibung, double preis, String adresse,
			String username) throws IOException {
		UserManagementDAO udao = new UserManagementDAO();
		if (udao.getUserbyUsername(username) != null) {
			Location l = ldao.getLocationbyID(id);
			if(!(name.isEmpty()))
			{
				l.setName(name);
			}
			if(maxTeilnehmer != 0)
			{
				l.setMaxTeilnehmer(maxTeilnehmer);
			}
			if(!(type.isEmpty()))
			{
				l.setType(type);
			}
			if(!(beschreibung.isEmpty()))
			{
				l.setBeschreibung(beschreibung);
			}
			if(preis != 0.0)
			{
				l.setPreis(preis);
			}
			if(!(adresse.isEmpty()))
			{
				l.setAdresse(adresse);
			}
			ldao.updateLocation(l);
		} else {
			throw new IllegalArgumentException("Der User konnte nicht gefunden werden.");
		}

	}

	/**
	 * Angegeben Lokation wird gelöscht
	 * 
	 * @param id
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void deleteLocation(String id) throws IllegalArgumentException, IOException {
		EventManagement em = new EventManagement();
		ArrayList<Event> arr_event = em.getAllEvents();
		for(Event e : arr_event)
		{
			if(e.getLocation_id() == Integer.parseInt(id))
			{
				throw new IllegalArgumentException("Löschen der Location fehlgeschlagen! In dieser Location findet ein Event statt!");
			}
		}
		ldao.deleteLocation(id);

	}

	/**
	 * Liefert alle möglichen Lokations
	 * 
	 * @param datum
	 * @param name
	 * @param typ
	 * @return Liste aller treffenden Lokations
	 * @throws IOException 
	 */
	public ArrayList<Location> findAvailableLocations(String datum,
			String name, String typ) throws IOException {

		SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
		Date pdatum = null;
		try {
			pdatum = sd.parse(datum);
			datum = sd.format(pdatum);
		}
		catch (Exception e) 
		{
			throw new IllegalArgumentException(
					"Bitte geben Sie das Datum im Format TT.MM.JJJJ ein.");
		}
		
		if (pdatum.before(new Date())) 
		{
			throw new IllegalArgumentException(
					"Ihr Datum liegt in der Vergangenheit oder wurde nicht korrekt eingegeben! Bitte geben Sie das Datum im Format TT.MM.JJJJ ein.");
		}

		ArrayList<Location> arr_avaloc = new ArrayList<Location>();
		ArrayList<Location> arr_loc = getAllLocations();
		
		if(  name.isEmpty() && typ.isEmpty())
		{
				arr_avaloc = arr_loc;
		}
		else if(!(name.isEmpty()) && typ.isEmpty())
		{
			for (Location location : arr_loc) 
			{
				if(location.getName().toLowerCase().contains(name.toLowerCase()))
				{
					arr_avaloc.add(location);
				}
			}
		}
		else if(name.isEmpty() && !(typ.isEmpty()))
		{
			for (Location location : arr_loc) 
			{
				if(location.getType().equals(typ))
				{
					arr_avaloc.add(location);
				}
			}
		}
		else
		{
			for (Location location : arr_loc) 
			{
				if(location.getType().equals(typ) && location.getName().toLowerCase().contains(name.toLowerCase()))
				{
					arr_avaloc.add(location);
				}
			}
		}
		
		
		//vergebene Locations entfernen
		EventManagement em = new EventManagement();
		ArrayList<Event> arr_event = em.getAllEvents();

		for(int lc = 0; lc < arr_avaloc.size(); lc++){
			for(int ec = 0; ec < arr_event.size(); ec++){
				if(arr_avaloc.get(lc).getId() == arr_event.get(ec).getLocation_id()){
					if(arr_event.get(ec).getDatumToString().equals(datum)){
						arr_avaloc.remove(lc);
						break;
					}
				}
			}			
		}
		Collections.sort(arr_avaloc);		
		
		return arr_avaloc;
	}

	/**
	 * liefert alle Locations
	 * 
	 * @return Liste aller Locations
	 * @throws IOException 
	 */
	public ArrayList<Location> getAllLocations() throws IOException {

		ArrayList<Location> al =  ldao.getLocationList();
		Collections.sort(al);
		return al;
	}

	public Location getLocation(int id) throws IOException {
		ArrayList<Location> arrr_loc = getAllLocations();

		for (Location location : arrr_loc) {
			if (location.getId() == id) {
				return location;
			}
		}
		throw new NullPointerException("Die ausgewählte Location existiert nicht.");
	}
}
