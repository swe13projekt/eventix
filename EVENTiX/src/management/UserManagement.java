package management;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import users.Analyst;
import users.RegUser;
import users.Veranstalter;
import users.Vermieter;
import dao.UserManagementDAO;
import events.Event;
import locations.Location;
/**
 * <h2>UserManagement 
 * @author Dominic Böhm
 * @version 1.0
 * Klasse zum verwalten der Klasse {@link RegUser}. Bietet sozusagen die Funktionen damit Daten, welche aus dem {@link UserManagementDAO} dem {@link Frontend}zur Verfügung gestellt werden können
 *
 */
public class UserManagement {

	UserManagementDAO udao;
	/**
	 * Konstruktor
	 * @throws IOException 
	 */
	public UserManagement() throws IOException {
		udao = new UserManagementDAO();
	}
	/**
	 * Erzeugt einen neuen User
	 * @param username
	 * @param vorname
	 * @param nachname
	 * @param eMail
	 * @param geschlecht
	 * @param password
	 * @param gebDate
	 * @throws IOException 
	 * @throws IllegalArgumentException 
	 */
	public void createUser(String username, String vorname, String nachname,
			String eMail, String geschlecht, String password, String gebDate, String rolle, String checkpassword) throws IllegalArgumentException, IOException
	{
		
		String organisation = "";
		String adresse = "";
		String telNummer = "";
				
		if(!(password.equals(checkpassword))) 
			{
				throw new IllegalArgumentException("Die Passwörter stimmen nicht überein!");
			}
		

		SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
		Date regDate = new Date(); 
		Date gebDate2 = null;
		try {
			gebDate2 = sd.parse(gebDate);
		} catch (ParseException e) {
			
			throw new IllegalArgumentException("Bitte geben Sie das Datum im Format TT.MM.JJJJ ein.");
		} 
		
		// 0 = nur reguser, 1 = veranstalter, 2 = vermieter, 3 = analyst
		int r = Integer.parseInt(rolle);
		RegUser user;
		switch (r) {
		case 1:
			user = new Veranstalter(username, vorname, nachname, eMail, geschlecht.charAt(0), password, regDate, gebDate2, organisation, adresse, telNummer);
			break;
		case 2:
			user = new Vermieter(username, vorname, nachname, eMail, geschlecht.charAt(0), password, regDate, gebDate2, organisation, adresse, telNummer);
			break;
		case 3:
			user = new Analyst(username, vorname, nachname, eMail, geschlecht.charAt(0), password, regDate, gebDate2, organisation);
			break;
		default:
			user = new RegUser(username, vorname, nachname, eMail, geschlecht.charAt(0), password, regDate , gebDate2);
			break;
		}
		udao.addUser(user);

	}
	/**
	 * Lässt einen bestehenden User verändern
	 * @param username
	 * @param vorname
	 * @param nachname
	 * @param eMail
	 * @param geschlecht
	 * @param password
	 * @throws IOException 
	 */
	public void editUser(String username, String vorname, String nachname,
			String eMail, String geschlecht, String password) throws IOException
	{
		// vLukas: gebDate kann nicht geaendert werden
		RegUser user = udao.getUserbyUsername(username);
		
		if(!(vorname.isEmpty()))
		{
			user.setVorname(vorname);
		}
		if(!(nachname.isEmpty()))
		{
			user.setNachname(nachname);
		}
		if(!(eMail.isEmpty()))
		{
			user.seteMail(eMail);
		}
		if(!(geschlecht.isEmpty()))
		{
			user.setGeschlecht(geschlecht.charAt(0));
		}
		if(!(password.isEmpty()))
		{
			user.setPassword(password);
		}
		
		udao.updateUser(user);
	}
	/**
	 * Gibt spezielle Statistiken in einem zwei dimensionalen Array aus, wobei array[x][0] immer der Name des Wertes ist und
	 * array[x][1] der dazugehörige werte
	 * @return Liste aller Name + Statistikwerte in einem 2-Dim Array 
	 */
	public String[][] gibStatistik()
	{
		String[][] erg = new String[18][2];
		StatistikManagement stm = new StatistikManagement();
		try {
			erg[ 0][0] = "Anzahl der registrierter User";
			erg[ 0][1] = stm.gibAnzRegUser()+"";
			erg[ 1][0] = "Prozent weiblicher User";
			erg[ 1][1] = stm.gibProzentWeiblich()+"";
			erg[ 2][0] = "Prozent männlicher User";
			erg[ 2][1] = stm.gibProzentMannlich()+"";
			erg[ 3][0] = "Anzahl der Veranstalter";
			erg[ 3][1] = stm.gibAnzVeranstalter()+"";
			erg[ 4][0] = "Prozent der Veranstalter";
			erg[ 4][1] = stm.gibProzentVeranstalter()+"";
			erg[ 5][0] = "Anzahl der Vermieter";
			erg[ 5][1] = stm.gibAnzVermieter()+"";
			erg[ 6][0] = "Prozent der Vermieter";
			erg[ 6][1] = stm.gibProzentVermieter()+"";
			erg[ 7][0] = "Anzahl der Analysten";
			erg[ 7][1] = stm.gibAnzAnalyst()+"";
			erg[ 8][0] = "Prozent der Analysten";
			erg[ 8][1] = stm.gibProzentAnalyst()+"";
			erg[ 9][0] = "Anzahl der normalen User";
			erg[ 9][1] = stm.gibAnzNormale()+"";
			erg[10][0] = "Prozent der normalen User";
			erg[10][1] = stm.gibProzentNormaler()+"";
			erg[11][0] = "Anzahl der Events";
			erg[11][1] = stm.gibAnzEvent()+"";
			erg[12][0] = "Anzahl der Locations";
			erg[12][1] = stm.gibAnzLocation()+"";
			erg[13][0] = "Durchschnittliche Anzahl Teilnehmer pro Event";
			erg[13][1] = stm.gibDurchschnittTeilnehmerProEvent()+"";
			erg[14][0] = "Durchschnittliche Anzahl Events pro Location";
			erg[14][1] = stm.gibDurchschnittEventProLocation()+"";
			erg[15][0] = "Durchschnittlicher Preis pro Location";
			erg[15][1] = stm.gibDurchschnittPreisProLocation()+"";
			erg[16][0] = "Durchschnittlicher Preis pro Event";
			erg[16][1] = stm.gibDurchschnittPreisProEvent()+"";
			erg[17][0] = "Durchscnittliche Kapazität pro Location";
			erg[17][1] = stm.gibDurchschnittKapaProLocation()+"";

		} catch (Exception e) {
		
			e.printStackTrace();
		}
		return erg;		
	}
	/**
	 * Gibt die Userbezogenen Locations (Vermieter)
	 * @param username
	 * @return Liste der Locations des Vermieters
	 * @throws IOException 
	 */
	public ArrayList<Location> getMyLocations(String username) throws IOException
	{
		RegUser user = udao.getUserbyUsername(username);
		LocationManagement lm = new LocationManagement();
		ArrayList<Location> arr_location = lm.getAllLocations();
		ArrayList<Location> arr_mylocations = new ArrayList<Location>();
		for(Location l : arr_location)
		{
			if(l.getBesitzer().equals(user.getUsername()))
			{
				arr_mylocations.add(l);
			}
		}
		Collections.sort(arr_mylocations);
		return arr_mylocations;
		
	}
	/**
	 * Gibt die Events an dem der User teilnimmt. 
	 * @param username
	 * @return liste aller Events an dem der User teilnimmt
	 * @throws IOException 
	 */
	public ArrayList<Event> getMyCalender(String username) throws IOException
	{
		RegUser user = null;
		
		try{
			user = udao.getUserbyUsername(username);
		}
		catch(NullPointerException e){
			throw new IllegalArgumentException("Nullpointer.");
		}
		catch(Exception e){
			throw new IllegalArgumentException(e.getMessage());
		}
		
		EventManagement em = new EventManagement();
		ArrayList<Event> arr_events = em.getAllEvents();
		ArrayList<Event> arr_myevents = new ArrayList<Event>();
		Date today = new Date();
		
		for(Event e : arr_events)
		{
			if(!(e.getDatum().before(today))){
				ArrayList<RegUser> arr_teilnehmer = e.getTeilnehmer();
				
				for(RegUser u : arr_teilnehmer)
				{
					if(u.getUsername().equals(user.getUsername()))
					{
						arr_myevents.add(e);
					}
				}
			}
		}
		Collections.sort(arr_myevents);
		
		return arr_myevents;
		
	}
	/**
	 * gibt die Events des Veranstalters aus
	 * @param username
	 * @return Liste der Events die der Veranstalter anbietet
	 * @throws IOException 
	 */
	public ArrayList<Event> getMyEvents(String username) throws IOException
	{

		RegUser user = udao.getUserbyUsername(username);
		EventManagement em = new EventManagement();
		ArrayList<Event> arr_events = em.getAllEvents();
		ArrayList<Event> arr_myevents = new ArrayList<Event>();
		
		for(Event e : arr_events)
		{
				if(e.getVeranstalter().equals(user.getUsername()))
				{
					arr_myevents.add(e);
				}
		}
		Collections.sort(arr_myevents);
		return arr_myevents;
		
	}
	/**
	 * Gibt eine komplette Userliste zurück
	 * @return Liste mit Usern
	 * @throws IOException 
	 */
	public ArrayList<RegUser> getUserList() throws IOException 
	{
		return udao.getUserList();
	}
	/**
	 * Gibt eine speziellen User zurück
	 * @param username
	 * @return gefundenen User
	 * @throws IOException 
	 */
	public RegUser getUserByUsername(String username) throws IOException
	{
		RegUser myuser = udao.getUserbyUsername(username);
		if(myuser == null) 
			{
				throw new IllegalArgumentException("Es existiert kein User mit dem Username " + username + ".");
			}
		
		return udao.getUserbyUsername(username);
	}
	/**
	 * logt einen User ein
	 * @param username
	 * @param password
	 * @return
	 * @throws IOException 
	 */
	public RegUser login(String username, String password) throws IOException
	{

		RegUser tologin = getUserByUsername(username);
		if(tologin.getPassword().equals(password))
		{
			return tologin; 
		} 
		else
		{
			throw new IllegalArgumentException("Ihre Logindaten sind leider nicht korrekt!");
		}
		
	}
//	public void logout()
//	{
//		//Logout uebernimmt das Frontend
//	}
}
