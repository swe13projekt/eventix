package tests;

import static org.junit.Assert.*;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;
import dao.LocationManagementDAO;
import locations.Location;

public class LocationManagementDAOTest 
{
	SimpleDateFormat formatter = new SimpleDateFormat( "dd.MM.yyyy" );
	
	//Location
	private Location location = null;
	private int id = 1;
	private String name = "locationname";
	private int maxTeilnehmer = 100;
	private String type = "Sonstiges";
	private String beschreibung = "Das ist die Beschreibung zu locationname";
	private double preis = 10.50;
	private String adresse = "locationadresse 1, 1000 Wien";
	private String besitzer = "username";
	
	public void setLocation()
	{
		location = new Location( id, name, maxTeilnehmer, type, beschreibung, preis, adresse, besitzer );
	}
	

	@Before
	public void setUp()
	{
		setLocation();
	}
	
	public String toString( Location location )
	{
		String locationString = location.getId() + ", " +
				location.getName() + ", " +
				location.getMaxTeilnehmer() + ", " +
				location.getType() + ", " +
				location.getBeschreibung() + ", " +
				location.getPreis() + ", " +
				location.getAdresse() + ", " +
				location.getBesitzer();
		
		return locationString;
	}
	
	/**
	 * Testen ob eine Location angelegt und auch wieder ausgegeben werden kann
	 */
	@Test
	public void addAndGetByLocationID()
	{
		try
		{
			LocationManagementDAO locdao = new LocationManagementDAO();
			
			locdao.flushFile( "locations.ser" );
			
			locdao.addLocation( location );
			
			Location locationFromDAO = locdao.getLocationbyID( location.getId() );

			assertEquals( toString( locationFromDAO ), toString( location ) );
	
		} catch( IOException e )
		{
			System.err.println( e );
		}
	}
	
	/**
	 * Testet ob Informationen einer Location aktualisiert werden können (in diesem Fall die Beschreibung)
	 */
	@Test
	public void updateLocation()
	{
		try
		{
			LocationManagementDAO locdao = new LocationManagementDAO();
			
			if( locdao.getLocationList().isEmpty() )
				addAndGetByLocationID();
			
			location.setBeschreibung( "Das ist eine neue Beschreibung mit allem drum und dran" );
			locdao.updateLocation( location );
			
			Location locationFromDAO = locdao.getLocationbyID( location.getId() );
			
			assertEquals( toString( locationFromDAO ), toString( location ) );
			
		} catch( IOException e )
		{
			System.err.println( e );
		}	
	}
	
	/**
	 * Testet ob eine Location mit der selben ID angelegt werden kann
	 * Es wird eine IllegalArgumentException erwartet
	 */
	@Test(expected=IllegalArgumentException.class)
	public void addSameLocationIDTwiceFailure()
	{
		try
		{
			LocationManagementDAO locdao = new LocationManagementDAO();
			
			if( locdao.getLocationList().isEmpty() )
				addAndGetByLocationID();
			
			locdao.addLocation( location );
			
		} catch( IOException e)
		{
			System.err.println( e );
		}
	}
	
	/**
	 * Testet ob die Location gelöscht werden kann
	 */
	@Test
	public void deleteLocation()
	{
		try
		{
			LocationManagementDAO locdao = new LocationManagementDAO();
			
			locdao.flushFile( "locations.ser" );
			
			addAndGetByLocationID();
			
			locdao.deleteLocation( Integer.toString( location.getId() ) );

			assertEquals( locdao.getLocationList().size(), 0 );
			
		} catch( IOException e )
		{
			System.err.println( e );
		} catch( Exception e )
		{
			System.err.println( e );
		}
	}
	
	/**
	 * Testet ob eine Veranstaltung zweimal gelöscht werden kann.
	 * Das erwartete Ergebnis ist eine IllegalArgumentExceptin.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void deleteLocationFailure()
	{
		try
		{
			LocationManagementDAO locdao = new LocationManagementDAO();
			
			deleteLocation();
			
			locdao.deleteLocation( Integer.toString( location.getId() ) );
			
		} catch( IOException e )
		{
			System.err.println( e );
		}
	}
	

}
