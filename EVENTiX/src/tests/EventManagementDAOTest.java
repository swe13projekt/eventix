package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import dao.EventManagementDAO;
import events.Event;
import users.RegUser;

public class EventManagementDAOTest
{
	SimpleDateFormat formatter = new SimpleDateFormat( "dd.MM.yyyy" );
	
	//Event
	private Event event = null;
	private int eventid = 1;
	private String eventname = "eventname";
	private int locationid = 1;
	private String date = "12.12.2013";
	private int maxTeilnehmer = 10;
	private double preis = 9.50;
	private String beschreibung = "Dies ist die Beschreibung zu Eventname";
	private String beginnzeit = "20:00 Uhr";
	private ArrayList<RegUser> teilnehmer;
	private String veranstalter = "username";
	
	private void setEvent()
	{
		event = new Event( eventid, eventname, locationid, stringToDate(date), maxTeilnehmer,
				preis, beschreibung, beginnzeit, teilnehmer, veranstalter );
	}
	
	
	private Date stringToDate( String dat )
	{	
		Date date = null;
		try
		{
			date = (Date)formatter.parse( dat );
		} catch (Exception e) {}
		return date;
	}
	
	@Before
	public void setUp()
	{
		setEvent();
	}
	
	public String toString( Event event )
	{
		String eventString = event.getId() + ", " +
				event.getName() + ", " +
				event.getLocation_id() + ", " +
				event.getDatumToString() + ", " +
				event.getMaxTeilnehmer() + ", " +
				event.getPreis() + ", " +
				event.getBeschreibung() + ", " +
				event.getBeginnzeit() + ", " +
				event.getTeilnehmer() + ", " +
				event.getVeranstalter();
		
		return eventString;
	}
	
	/**
	 * Testen ob eine Veranstaltung angelegt und auch wieder ausgegeben werden kann
	 */
	@Test
	public void addAndGetByEventID()
	{
		try
		{
			EventManagementDAO evdao = new EventManagementDAO();
			
			evdao.flushFile( "events.ser" );
			
			evdao.addEvent( event );
			
			Event eventFromDAO = evdao.getEventbyID( event.getId() );

			assertEquals( toString( eventFromDAO ), toString( event ) );
	
		} catch( IOException e )
		{
			System.err.println( e );
		}
	}
	
	/**
	 * Testet ob Informationen einer Veranstaltung aktualisiert werden können (in diesem Fall die Beschreibung)
	 */
	@Test
	public void updateEvent()
	{
		try
		{
			EventManagementDAO evdao = new EventManagementDAO();
			
			if( evdao.getEventList().isEmpty() )
				addAndGetByEventID();
			
			event.setBeschreibung( "Das ist eine neue Beschreibung mit allem drum und dran" );
			evdao.updateEvent( event );
			
			Event eventFromDAO = evdao.getEventbyID( event.getId() );
			
			assertEquals( toString( eventFromDAO ), toString( event ) );
			
			
		} catch( IOException e )
		{
			System.err.println( e );
		}	
	}
	
	/**
	 * Testet ob eine Veranstaltung mit der selben ID angelegt werden kann
	 * Es wird eine IllegalArgumentException erwartet
	 */
	@Test(expected=IllegalArgumentException.class)
	public void addSameEventIDTwiceFailure()
	{
		try
		{
			EventManagementDAO evdao = new EventManagementDAO();
			
			if( evdao.getEventList().isEmpty() )
				addAndGetByEventID();
			
			evdao.addEvent( event );
			
		} catch( IOException e)
		{
			System.err.println( e );
		}
	}
	
	/**
	 * Testet ob die Veranstaltung gelöscht werden kann
	 */
	@Test
	public void deleteEvent()
	{
		try
		{
			EventManagementDAO evdao = new EventManagementDAO();
			
			evdao.flushFile( "events.ser" );
			
			addAndGetByEventID();
			
			evdao.deleteEvent( Integer.toString( event.getId() ) );

			assertEquals( evdao.getEventList().size(), 0 );
			
		} catch( IOException e )
		{
			System.err.println( e );
		} catch( Exception e )
		{
			System.err.println( e );
		}
	}
	
	/**
	 * Testet ob eine Veranstaltung zweimal gelöscht werden kann.
	 * Das erwartete Ergebnis ist eine IllegalArgumentExceptin.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void deleteEventFailure()
	{
		try
		{
			EventManagementDAO evdao = new EventManagementDAO();
			
			deleteEvent();
			
			evdao.deleteEvent( Integer.toString( event.getId() ) );
			
		} catch( IOException e )
		{
			System.err.println( e );
		}
	}
	

}
