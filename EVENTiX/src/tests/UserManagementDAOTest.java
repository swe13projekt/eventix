package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import users.RegUser;
import dao.UserManagementDAO;

/**
 * @author Andreas Fuchs
 * Testet die Klasse UserManagementDAO aus dem Package dao.
 */


public class UserManagementDAOTest
{	
	SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
	
	RegUser reguser;
	String username = "username";
	String vorname = "vorname";
	String nachname = "nachname";
	String email = "e@mail.com";
	char sex = 'w';
	String password = "sicherespw";
	String regDate = "30.11.2013";
	String gebDate = "18.09.1983";
	UserManagementDAO umdao;
	
	
	public void setRegUser()
	{
		reguser = new RegUser(username, vorname, nachname, email, sex, password, stringToDate(regDate), stringToDate(gebDate));
	}
	
	private Date stringToDate( String dat )
	{	
		Date date = null;
		try
		{
			date = (Date)formatter.parse( dat );
		} catch (Exception e) {}
		return date;
	}
	
	
	@Before
	public void setUp()
	{
		setRegUser();
	}
	
	public String toString( RegUser reguser )
	{
		String userString = reguser.getUsername() + ", " +
				reguser.getVorname() + ", " +
				reguser.getNachname() + ", " +
				reguser.geteMail() + ", " +
				reguser.getGeschlecht() + ", " +
				reguser.getPassword() + ", " +
				reguser.getRegDate().toString() + ", " +
				reguser.getGebDate().toString();
		
		return userString;
	}
	
	/**
	 * Testet ob der Benutzer gespeichert und wieder abgerufen werden kann
	 */
	@Test
	public void addAndGetByUsername()
	{
		try
		{
			UserManagementDAO umdao = new UserManagementDAO();
			
			umdao.flushFile( "users.ser" );
			
			umdao.addUser( reguser );
			
			RegUser reguserFromDAO = umdao.getUserbyUsername( reguser.getUsername() );

			assertEquals(toString(reguserFromDAO), toString(reguser));
			
		}catch( IOException e )
		{
			System.err.println( e );
		}
	}

	/**
	 * Testet ob Informationen des Benutzers aktualisiert werden könne (in diesem Fall das Passwort)
	 */
	@Test
	public void updateUser()
	{
		try
		{
			UserManagementDAO umdao = new UserManagementDAO();
			
			if( umdao.getUserList().isEmpty() )
				addAndGetByUsername();
			
			
			reguser.setPassword( "nochsicherersPW" );
			umdao.updateUser( reguser );
			
			RegUser reguserFromDAO = umdao.getUserbyUsername( reguser.getUsername() );
			
			assertEquals( toString( reguserFromDAO ), toString( reguser ) );
			
			
		} catch( IOException e )
		{
			System.err.println( e );
		}
		
	}
	
	/**
	 * Testet ob der selbe Benutzer zwei mal angelegt werden kann.
	 * Das erwartete Ergebnis ist eine IllegalArgumentException
	 */
	@Test(expected=IllegalArgumentException.class)
	public void addSameUserTwiceFailure()
	{
		try
		{
			UserManagementDAO umdao = new UserManagementDAO();
			
			if( umdao.getUserList().isEmpty() )
				addAndGetByUsername();
			
			umdao.addUser( reguser );
			
		} catch( IOException e)
		{
			System.err.println( e );
		}
	}
	
	/**
	 * Testet ob der Benutzer gelöscht werden kann.
	 * Dazu wird eine Liste erstellt, ein Benutzer hinzugefügt und anschliesend der Benutzer wieder gelöscht.
	 * Die ArrayList sollte anschließend wieder leer sein.
	 */
	@Test
	public void deleteUser()
	{
		try
		{
			UserManagementDAO umdao = new UserManagementDAO();
			
			umdao.flushFile( "users.ser" );
			
			addAndGetByUsername();
			
			umdao.deleteUser(reguser.getUsername());
			
			assertEquals( umdao.getUserList().size(), 0 );
			
		} catch( IOException e )
		{
			System.err.println( e );
		}
	}
	
	/**
	 * Testet ob ein Benutzer zweimal gelöscht werden kann.
	 * Das erwartete Ergebnis ist eine IllegalArgumentExceptin.
	 */
	@Test(expected=IllegalArgumentException.class)
	public void deleteUserFailure()
	{
		try
		{
			UserManagementDAO umdao = new UserManagementDAO();
			
			deleteUser();
			
			umdao.deleteUser( reguser.getUsername() );
			
		} catch( IOException e )
		{
			System.err.println( e );
		}
	}
	
}
