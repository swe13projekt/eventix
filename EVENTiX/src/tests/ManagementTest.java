package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import locations.Location;
import events.Event;
import management.*;
import users.*;
import dao.*;


/**
 * Diese Klasse führt diverse Tests mit den Management Klassen durch ob diese die Angeboten funktionalität erfüllen
 * @author fuchsy
 *
 */
public class ManagementTest 
{
	SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
	RegUser[] regusers;	
	Veranstalter[] veranstalter;
	Vermieter[] vermieter;
	Analyst analyst;
	Event events[];
	Location locations[];
	
	
	/**
	 * Initialisiert die User
	 */
	public void setUpUsers()
	{
		regusers = new RegUser[5];
		veranstalter = new Veranstalter[2];
		vermieter = new Vermieter[2];
		
		String username, vorname, nachname, email, password, regDate, gebDate, organisation, adresse, telnummer;
		char sex;
		
		//RegUsers
		for( int i = 0; i < regusers.length; ++i )
		{
			char c = ( Character.toChars( i + 65 ))[0];
			username = "regusername" + c;
			vorname = "regvorname" + c;
			nachname = "regnachname" + c;
			email = "reg" + c + "@email.com";
			if (i % 2 == 0)
				sex = 'w';
			else 
				sex = 'm';
			password = "passwort" +c;
			//Dass das regDate vor dem gebDate sein kann wird hier bewusst ignoriert
			regDate = randompastdate();
			gebDate = randombirthdate();
			
			RegUser ru = new RegUser(username, vorname, nachname, email, sex, password, 
					stringToDate(regDate), stringToDate(gebDate));
			regusers[i] = ru;
		}
		
		//Veranstalter
		for( int i = 0; i < veranstalter.length; ++i )
		{
			char c = ( Character.toChars( i + 65 ) )[0];
			username = "veranusername" + c;
			vorname = "veranvorname" + c;
			nachname = "verannachname" + c;
			email = "veran" + c + "@email.com";
			if (i % 2 == 0)
				sex = 'w';
			else 
				sex = 'm';
			password = "passwort" +c;
			//Dass das regDate vor dem gebDate sein kann wird hier bewusst ignoriert
			regDate = randompastdate();
			gebDate = randombirthdate();
			organisation = "Veranstalter Org" + c;
			adresse = "Strasse " + i + ", " + ( i * 1013 )% 10000  + " Stadt";
			Random rand = new Random();
			telnummer = String.valueOf( rand.nextInt() );
			
			Veranstalter va = new Veranstalter(username, vorname, nachname, email, sex, password, 
					stringToDate(regDate), stringToDate(gebDate), organisation, adresse, telnummer);
			veranstalter[i] = va;
		}
		
		//Vermieter
		for( int i = 0; i < vermieter.length; ++i )
		{
			char c = ( Character.toChars( i + 65 ) )[0];
			username = "vermieusername" + c;
			vorname = "vermievorname" + c;
			nachname = "vermienachname" + c;
			email = "vermie" + c + "@email.com";
			if ( i % 2 == 0 )
				sex = 'w';
			else 
				sex = 'm';
			password = "passwort" + c;
			//Dass das regDate vor dem gebDate sein kann wird hier bewusst ignoriert
			regDate = randompastdate();
			gebDate = randombirthdate();
			organisation = "Vermieter Org" + c;
			adresse = "Strasse " + i + ", " + ( i * 1013 )% 10000  + " Stadt";
			Random rand = new Random();
			telnummer = String.valueOf( rand.nextInt() );

			Vermieter vm = new Vermieter(username, vorname, nachname, email, sex, password, 
					stringToDate(regDate), stringToDate(gebDate), organisation, adresse, telnummer);
			vermieter[i] = vm;
		}
		
		// Analyst
		username = "anausername";
		vorname = "anavorname";
		nachname = "ananachname";
		email = "ana@email.com";
		sex = 'm';
		password = "passwort";
		//Dass das regDate vor dem gebDate sein kann wird hier bewusst ignoriert
		regDate = randompastdate();
		gebDate = randombirthdate();
		organisation = "Analyst Org";
		
		analyst = new Analyst(username, vorname, nachname, email, sex, password, 
				stringToDate(regDate), stringToDate(gebDate), organisation);
	}
	
	/**
	 * Initialisiert die Locations
	 * Wichtig: setUpUsers sollte vorher ausgeführt werden!
	 */
	public void setUpLocations()
	{
		locations = new Location[3];
		
		int id, maxTeilnehmer;
		String name, type, beschreibung, adresse, besitzer;
		double preis;
		
		try
		{
			for( int i = 0; i < locations.length; ++i )
			{
				id = i;
				name = "LocationName" + i;
				maxTeilnehmer = randomNumBetween( 10, 100 );
				type = "Location vom Type" + i;
				beschreibung = "Location Beschreibung" + i;
				preis = (double)randomNumBetween( 10, 100 ) / (double)randomNumBetween( 1, 10 );
				adresse = "Strasse " + i + ", " + ( i * 1013 )% 10000  + " Stadt";
				int j = randomNumBetween( 0, this.vermieter.length );
				besitzer = this.vermieter[j].getUsername();
				
				Location location = new Location( id, name, maxTeilnehmer, type, beschreibung, preis, adresse, besitzer );
				
				locations[i] = location;
			}
			
		} catch( Exception e )
		{ 
			System.err.println( e.getMessage() );
		}
	}	
	
	
	/**
	 * Initialisiert die Event
	 * Wichtig: Es sollte setupUsers() und setupLocations() vorher ausgeführt werden!
	 */
	public void setUpEvents()
	{
		events = new Event[3];
		
		String name, beschreibung, beginnzeit, veranstalter;
		int id, maxTeilnehmer, location_id;
		double preis;
		Date datum;
		ArrayList<RegUser> teilnehmer = null;
		
		try
		{
			for( int i = 0; i < events.length; ++i )
			{
				id = i;
				name = "Eventname" + i;
				
				int j = randomNumBetween( 0, this.locations.length );
				location_id = this.locations[j].getId();

				//Sicher gehen dass nicht zufällig ein Event am selben Ort zur selben Zeit angelegt wird
				datum = formatter.parse( randomfuturedate() );
				for( j = 0; j < events.length; ++j )
				{
					if( events[j] != null )
					{
						if( events[j].getLocation_id() == location_id )
						{
							while( events[j].getDatum() == datum )
								datum = formatter.parse( randomfuturedate() );
						}
					}
				}
				maxTeilnehmer = randomNumBetween( 10, 1000 );
				preis = (double)randomNumBetween( 0, 100 ) / (double)randomNumBetween( 1,10 );
				beschreibung = "Event Beschreibung " + i;
				beginnzeit = String.valueOf( randomNumBetween( 0, 23 ) ) + ":" + String.valueOf( randomNumBetween( 0, 59 ) );

				j = randomNumBetween( 0, this.veranstalter.length );
				veranstalter = this.veranstalter[j].getUsername();
				
				Event event = new Event( id, name, location_id, datum, maxTeilnehmer, preis, beschreibung, beginnzeit, 
						teilnehmer, veranstalter );
				
				events[i] = event;	
			}
		} catch( Exception e )
		{
			System.err.println( e.getMessage() );
		}
	}
	
	public String userToString( RegUser reguser )
	{
		String userString = reguser.getUsername() + ", " +
				reguser.getVorname() + ", " +
				reguser.getNachname() + ", " +
				reguser.geteMail() + ", " +
				reguser.getGeschlecht() + ", " +
				reguser.getPassword() + ", " +
				// Registrierungsdatum wird nicht überprüft da von System selbst erstellt.
				//reguser.getRegDate().toString() + ", " +
				reguser.getGebDate().toString();
		
		return userString;
	}
	
	public String locationToString( Location lo )
	{
		String locationString = lo.getName() + ", " +
				lo.getMaxTeilnehmer() + ", " +
				lo.getType() + ", " +
				lo.getBeschreibung() + ", " +
				lo.getPreis() + ", " +
				lo.getAdresse() + ", " +
				lo.getBesitzer();
		
		return locationString;
	}
	
	public String eventToString( Event ev )
	{
		String eventString = ev.getName() + ", " +
				ev.getDatumToString() + ", " +
				ev.getLocation_id() + ", " +
				ev.getMaxTeilnehmer() + ", " +
				ev.getPreis() + ", " + 
				ev.getBeschreibung() + ", " + 
				ev.getBeginnzeit() + ", " +
				ev.getVeranstalter();
		
		return eventString;
	}
	
	
	/**
	 * Gleicht die ID der Locations mit jenen die vom Management vergeben wurden ab.
	 * Es wird hier Angenommen dass die Name zur Identifikation der Location ausreichen.
	 * Die Tests sollten dementsprechend darauf ausgelegt sein. 
	 */
	public void syncLocationID(  )
	{
		try
		{
			LocationManagementDAO lmdao = new LocationManagementDAO();
			
			ArrayList<Location> locationlist = lmdao.getLocationList();
			
			for( int i = 0; i < locations.length; ++i )
			{
				for( int j = 0; j < locationlist.size(); ++j )
				{
					if ( locations[i].getName().equals( locationlist.get( j ).getName() ) )
					{
						int oldID = locations[i].getId();
						int newID = locationlist.get( j ).getId();
						locations[i].setId( newID  );
						for( int k = 0; k < events.length; ++k )
						{
							if( events[k].getLocation_id() == oldID )
								events[k].setLocation_id( newID );
						}
					}
				} 
			}
		} catch( Exception e )
		{
			System.err.println( e.getMessage() );
		}
	}
	
	/**
	 * Gleicht die ID der Events mit jenen die vom Management vergeben wurden ab.
	 * Es wird hier Angenommen dass die Name zur Identifikation des Events ausreichen.
	 * Die Tests sollten dementsprechend darauf ausgelegt sein. 
	 */
	public void syncEventID()
	{
		try
		{
			EventManagementDAO emdao = new EventManagementDAO();
			
			ArrayList<Event> eventlist = emdao.getEventList();
			
			for( int i = 0; i < events.length; ++i )
			{
				for( int j = 0; j < eventlist.size(); ++j )
				{
					if( events[i].getName().equals( eventlist.get( j ).getName() ) )
						events[i].setId( eventlist.get( j ).getId() );
				}
			}
		} catch( Exception e )
		{
			System.err.println( e.getMessage() );
		}
	}
	
	private Date stringToDate( String dat )
	{	
		Date date = null;
		try
		{
			date = (Date)formatter.parse( dat );
		} catch (Exception e) {}
		return date;
	}

	private int randomNumBetween(int start, int end)
	{
		int ranNum = start + (int)(Math.random() * ( end - start ));
		
		return ranNum;
	}
		
	@SuppressWarnings("static-access")
	private String randombirthdate()
	{
		GregorianCalendar gc = new GregorianCalendar();
        
        int year = randomNumBetween(1900, (gc.get(gc.YEAR))-18);
        gc.set(gc.YEAR, year);
        
        int dayOfYear = randomNumBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
        gc.set(gc.DAY_OF_YEAR, dayOfYear);
		
        String pastdate = formatter.format( gc.getTime() );
        return pastdate;
	}
	
	@SuppressWarnings("static-access")
	private String randompastdate()
	{
		GregorianCalendar gc = new GregorianCalendar();
        
        int year = randomNumBetween(gc.get(gc.YEAR)-3, gc.get(gc.YEAR)-1 );
        gc.set(gc.YEAR, year);
        
        int dayOfYear = randomNumBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
        gc.set(gc.DAY_OF_YEAR, dayOfYear);
		
        String pastdate = formatter.format( gc.getTime() );
        return pastdate;
		
	}
	
	@SuppressWarnings("static-access")
	private String randomfuturedate()
	{
		GregorianCalendar gc = new GregorianCalendar();
        
        gc.set(gc.YEAR, gc.get(gc.YEAR)+1);
        
        int dayOfYear = randomNumBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
        gc.set(gc.DAY_OF_YEAR, dayOfYear);
		
        String pastdate = formatter.format( gc.getTime() );
        return pastdate;
	}
	
	@Before
	public void setUp()
	{
		try
		{
			setUpUsers();
			setUpLocations();
			setUpEvents();
			
			// lösche zuerst Datenbank sodass doppelte Einträge vermieden werden
			UserManagementDAO umdao = new UserManagementDAO();
			umdao.flushFile( "users.ser" );
			LocationManagementDAO lmdao = new LocationManagementDAO();
			lmdao.flushFile( "locations.ser" );
			EventManagementDAO evdao = new EventManagementDAO();
			evdao.flushFile( "events.ser" );	
		} catch ( Exception e )
		{
			System.err.println( e.getMessage() );
		}
	}

	/**
	 * Testet das Erzeugen von Usern und ob diese wieder richtig zurückgegeben werden können
	 */
	@Test
	public void createUser() 
	{
		try
		{	
			UserManagement um = new UserManagement();
			RegUser re;
			Veranstalter va;
			Vermieter vm;
			Analyst an;
		
			for(int i = 0; i < regusers.length; ++i )
			{
				re = regusers[i];
				
				um.createUser(re.getUsername(), re.getVorname(), re.getNachname(), re.geteMail(), 
						String.valueOf( re.getGeschlecht() ), re.getPassword(), formatter.format( re.getGebDate() ), "4", re.getPassword() );
			}
			
			for(int i = 0; i < veranstalter.length; ++i )
			{	
				va = veranstalter[i];
				
				um.createUser(va.getUsername(), va.getVorname(), va.getNachname(), va.geteMail(), 
						String.valueOf( va.getGeschlecht() ), va.getPassword(), formatter.format( va.getGebDate() ), "1", va.getPassword() );
			}
			
			for(int i = 0; i < vermieter.length; ++i )
			{	
				vm = vermieter[i];
				
				um.createUser(vm.getUsername(), vm.getVorname(), vm.getNachname(), vm.geteMail(), 
						String.valueOf( vm.getGeschlecht() ), vm.getPassword(), formatter.format( vm.getGebDate() ), "2", vm.getPassword() );
			}
			
			an = analyst;
			um.createUser(an.getUsername(), an.getVorname(), an.getNachname(), an.geteMail(), 
					String.valueOf( an.getGeschlecht() ), an.getPassword(), formatter.format( an.getGebDate() ), "3", an.getPassword() );
			
			// Holle einen zufälligen User aus dem Management und vergleiche dessen Daten und Klasse
			
			int i = (int)Math.random() * regusers.length;
			RegUser regUserManagement = um.getUserByUsername( regusers[i].getUsername() );
			assertEquals( userToString( regUserManagement ), userToString( regusers[i] ) );
			assertTrue( regUserManagement instanceof RegUser );
			
			i = (int)Math.random() * veranstalter.length;
			RegUser veranManagement = um.getUserByUsername( veranstalter[i].getUsername() );
			assertEquals( userToString( veranManagement ), userToString( veranstalter[i] ) );
			assertTrue( veranManagement instanceof Veranstalter );
			
			i = (int)Math.random() * vermieter.length;
			RegUser vermieManagement = um.getUserByUsername( vermieter[i].getUsername() );
			assertEquals( userToString( vermieManagement ), userToString( vermieter[i] ) );
			assertTrue( vermieManagement instanceof Vermieter );
			
			RegUser anaManagement = um.getUserByUsername( analyst.getUsername() );
			assertEquals( userToString( anaManagement ), userToString( analyst ) );
			assertTrue( anaManagement instanceof Analyst );
						
			
		} catch (Exception e)
		{
			System.err.println( e.getMessage() );
			return;
		}
		
		
	}
	
	/**
	 * Testet das Erzeugen von Locations und ob diese wieder richtig zurückgegeben werden können
	 */
	@Test
	public void createLocations()
	{
		createUser();
		
		try
		{
			LocationManagement lm = new LocationManagement();
			Location lo;
			
			for( int i = 0; i < locations.length; ++i )
			{
				lo = locations[i];
				
				lm.createLocation(lo.getName(), lo.getMaxTeilnehmer(), lo.getType(), lo.getBeschreibung(), lo.getPreis(),
						lo.getAdresse(), lo.getBesitzer() );
			}
			
			syncLocationID();
			
			// die Locations testen
			for( int i = 0; i < locations.length; ++i )
			{
				Location locationMan = lm.getLocation( locations[i].getId() );
				assertEquals( locationToString( locationMan ), locationToString( locations[i] ) );
			}
			
		} catch( Exception e )
		{
			System.err.println( e.getMessage() );
		}
	}
	
	/**
	 * Testet das Erzeugen von Events und ob diese wieder richtig zurückgegeben werden können
	 */
	@Test
	public void createEvents()
	{
		createLocations();
		
		try
		{
			EventManagement em = new EventManagement();
			Event ev;
			
			for( int i = 0; i < events.length; ++i )
			{
				ev = events[i];
				
				em.createEvent(ev.getName(), ev.getLocation_id(), ev.getDatumToString(), String.valueOf( ev.getMaxTeilnehmer() ) , 
						String.valueOf( ev.getPreis() ), ev.getBeschreibung(), ev.getBeginnzeit(), ev.getVeranstalter() );
			}
			
			syncEventID();
			
			// Vergleichen ob die Events richtig gespeichert und wieder aufgerufen werden können
			for( int i = 0; i < events.length; ++i )
			{
				ev = events[i];
				Event evman = em.getEvent( ev.getId() );
				
				assertEquals( eventToString( evman ), eventToString( ev ));
			}
		} catch( Exception e )
		{
			System.err.println( e.getMessage() );
		}
	}
	
	/**
	 * Testet ob sich zu einem Event angemeldet werden kann.
	 */
	@Test
	public void registerEventTest()
	{
		createEvents();
		
		try
		{
			EventManagement em = new EventManagement();
			UserManagement um = new UserManagement();
			
			Event event = events[0];
			RegUser besucher = regusers[0];
			
			em.anmeldenZuEvent( besucher.getUsername(), String.valueOf( event.getId() ) );
			
			// Testet ob der Besucher im Event abgelegt wurde
			assertTrue( em.getEvent( event.getId() ).nimmtTeil( besucher.getUsername() ) );
			
			// Testet ob das Event in den Angemeldeten Events des Users gefunden werden kann
			ArrayList<Event> eventlist = um.getMyEvents( besucher.getUsername() );
			for( int i = 0; i < eventlist.size(); ++i )
				if( eventlist.get( i ).getId() == event.getId() )
					assertTrue( true );
			
		} catch( Exception e )
		{
			System.err.println( e.getMessage() );
		}
	
	}
	
	/**
	 * Testet ob sich vom angemeldeten Event wieder abgemeldet werden kann 
	 */
	@Test
	public void vonEventAbmeldenTest()
	{
		registerEventTest();
		
		try
		{
			EventManagement em = new EventManagement();
			UserManagement um = new UserManagement();
			
			Event event = events[0];
			RegUser besucher = regusers[0];
			
			em.abmeldenvonEvent( besucher.getUsername(), String.valueOf( event.getId() ) );
			
			// Testet ob der Besucher im Event entfernt wurde
			assertTrue( !(em.getEvent( event.getId() ).nimmtTeil( besucher.getUsername() )) );
			
			// Testet ob das Event in den Angemeldeten Events des Users nicht mehr gefunden werden kann
			ArrayList<Event> eventlist = um.getMyEvents( besucher.getUsername() );
			for( int i = 0; i < eventlist.size(); ++i )
				if( eventlist.get( i ).getId() == event.getId() )
					assertTrue( false );
			
			
		} catch( Exception e )
		{
			System.err.println( e.getMessage() );
		}
	}
	
	/**
	 * Testet ob doppelte Anmeldungen zu dem selben Event abgelehnt werden
	 * @throws IOException 
	 */
	@Test( expected = IllegalArgumentException.class )
	public void registerEventDoppelt()
	{
		registerEventTest();
		EventManagement em = null;
		
		try
		{
			em = new EventManagement();	
			Event event = events[0];
			RegUser besucher = regusers[0];
			
			em.anmeldenZuEvent( besucher.getUsername(), String.valueOf( event.getId() ) );
		} catch( IOException e )
		{
			System.err.println( e.getMessage() );
		} catch( IllegalArgumentException e )
		{
			throw new IllegalArgumentException();
		}
		
	}
	
	/**
	 * Testet ob nach eine Location per Name gesucht werden kann
	 */
	@Test
	public void locationFindenTest()
	{
		createLocations();
		
		try
		{
			LocationManagement lm = new LocationManagement();
			
			Location loc = locations[0];
			// Da noch keine Events angelegt wurden sollte die Location an einem beliebigen Datum noch frei sein
			String date = randomfuturedate();
			
			ArrayList<Location> locationlist = lm.findAvailableLocations(date, "", "");
			
			for( int i = 0; i < locationlist.size(); ++i )
			{
				if( locationlist.get( i ).getId() == loc.getId() )
					assertTrue( true );
			}
			
		} catch( Exception e )
		{
			System.err.println( e.getMessage() );
		}	
	}
	
	/**
	 * Testet ob das Löschen von Locations in dessen sich noch zukünftige Events befinden abgelehnt wird
	 */
	@Test( expected = IllegalArgumentException.class )
	public void deleteLocationFail()
	{
		createEvents();
		
		try
		{
			Event ev = events[0];
			int locid = ev.getLocation_id();
					
			LocationManagement lm = new LocationManagement();
			
			lm.deleteLocation( String.valueOf( locid ) );			
		} catch( IOException e )
		{
			System.err.println( e.getMessage() );
		} catch( IllegalArgumentException e )
		{
			throw new IllegalArgumentException();
		}
		
	}

}
