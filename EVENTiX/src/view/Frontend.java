package view;

import dao.EventManagementDAO;
import dao.LocationManagementDAO;
import dao.UserManagementDAO;
import events.Event;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.SingleThreadModel;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import users.RegUser;
import locations.Location;
import management.EventManagement;
import management.LocationManagement;
import management.UserManagement;

/**
 * 
 * Managen von Servlet und JSPs
 * 
 * @author      Lukas Moshuber (a1226124)
 * @version     1.0                  
 */
@SuppressWarnings("deprecation")
public class Frontend extends HttpServlet implements SingleThreadModel {
	
	/* Sicherheitsmechanismus:
	 * Alle JSPs muessen in den Array 'pages' eingetragen werden um gezieltes Zugreifen auf andere Files von Aussen zu verhindern.
	 * 'error' muss nicht eingetragen werden.
	 */
	
	private final String pages[] = {"home", "login", "signup", "mycalender", "myevents", "mylocations", "statistics", "eventsearch", "eventpage", "locationpage", "createlocation", "createevent", "findlocation", "signup_success", "logout_success", "createlocation_success", "delete_success", "createevent_success"};
	
	private static final long serialVersionUID = 1L;	
	private static RequestDispatcher JSPPage = null;
	
	private void doRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		
		String page = request.getParameter("page");
		String fkt = request.getParameter("fkt");
		
		HttpSession session = request.getSession(false);
					
		if(fkt != null){
// Login			
			if(fkt.equals("login")){
				doLogin(session, request, response);
			}
			
// Logout			
			if(fkt.equals("logout")){	
				session.invalidate();
				response.sendRedirect("index?page=logout_success");
			}
// Error Ausgabe, von JSP kommend			
			if(fkt.equals("error")){	
					doError(request, response);
			}
				
// Registrierung / Sign Up / Create User
			if(fkt.equals("signup")){			
				doSignup(request, response);
			}
// TestReset			
			if(fkt.equals("dbreset")){
				EventManagementDAO edao = new EventManagementDAO();
				edao.flushFile("events.ser");
				LocationManagementDAO ldao = new LocationManagementDAO();
				ldao.flushFile("locations.ser");
				UserManagementDAO udao = new UserManagementDAO();
				udao.flushFile("users.ser");	
				response.sendRedirect("index?page=home");
			}

//+ Nur für eingeloggte User
			if(session.getAttribute("username") != null){
				
// View - Eventpage			
				if(fkt.equals("vieweventpage")){
					doVieweventpage(request, response);
				}
				
// Eventsuche			
				if(fkt.equals("eventsearch")){
					doEventsearch(request, response);
				}				
	
// View - Locationpage		
				if(fkt.equals("viewlocationpage")){
					doViewlocationpage(request, response);
				}
				
	
// Eventteilnahme - Anmelden/Abmeldden			
				if(fkt.equals("eventteilnahme")){
					doEventteilnahme(session, request, response);
				}
				
//+ Nur fuer Veranstalter			
					if((Integer) session.getAttribute("status") == 1){
// Location suchen / finden						
											if(fkt.equals("findlocation")){
												doFindlocation(request, response);
											}
// Location gefunden -> Event anlegen											
											if(fkt.equals("createevent")){
												doCreateevent(session, request, response);
											}
											if(fkt.equals("deleteevent")){
												doDeleteevent(session, request, response);
											}
					}
					
//+ Nur fuer Vermieter			
					if((Integer) session.getAttribute("status") == 2){
// Location anlegen						
						if(fkt.equals("createlocation")){
							doCreatelocation(session, request, response);
						}
						if(fkt.equals("deletelocation")){
							doDeletelocation(session, request, response);
						}
					}
					
				
				
			}			
// Fehler abfangen, falls eine Methode aufgerufen wird, die nicht existiert.
			error("Die aufgerufene Methode " + fkt + " existiert nicht!", request, response);	
	}
		
		if(page == null || page.equals("")) page = "home"; 
		if(page == null || page.equals("") && session.getAttribute("username") != null) page = "mycalender";
				
		if(doesPageExist(page)){				
			printpage(page, request, response);	
		} else {
			error("Die Seite " + page + " konnte nicht gefunden werden!", request, response);
		}
		
		
	}
/**
*
* Login
*
* 
*/
public void doLogin(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	RegUser myuser = null;
	if(request.getParameter("user") == null || request.getParameter("password") == null){
		error("Es wurden nicht alle Variablen übergeben!", request, response);
	}
	
try{
		UserManagement um = new UserManagement();
		myuser = um.login(request.getParameter("user"), request.getParameter("password"));
	} catch(Exception e){
		error(e.getMessage(), request, response);
	}
	
	session = request.getSession(true);
	session.setAttribute("username", myuser.getUsername());
	session.setAttribute("status", myuser.getUserStatus());
	//session.setAttribute("username", "Test");
	//session.setAttribute("status", 1);
	response.sendRedirect("index?page=mycalender");		
}	
/**
*
* Eventteilnahme bearbeiten - Anmeldung/Abmeldung
*
* 
*/
public void doEventteilnahme(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	if(request.getParameter("kommt") == null || request.getParameter("id") == null){
		error("Es wurden nicht alle Variablen übergeben!", request, response);
	}
	int id = Integer.parseInt(request.getParameter("id"));
	EventManagement em = null;
	
	try{
		em = new EventManagement();
		Event myevent = em.getEvent(id);
		id = myevent.getId();			
	}catch(Exception e){
		error("Ein Event mit dieser ID existiert nicht.", request, response);
	}
	
	if(request.getParameter("kommt").equals("ja")){
		try{
			em.anmeldenZuEvent((String) session.getAttribute("username"), String.valueOf(id));
		} catch(Exception e){
			error(e.getMessage(), request, response);
		}
				
	} else if(request.getParameter("kommt").equals("nein")) {
		try{
			em.abmeldenvonEvent((String) session.getAttribute("username"), String.valueOf(id));
		} catch(Exception e){
			error(e.getMessage(), request, response);
		}
		
	} else { error("Es ist ein Fehler aufgetreten!", request, response); }
	
	request.setAttribute("id", id); 
	printpage("eventpage", request, response);	
}
/**
*
* Locationpage ausgeben
*
* 
*/
public void doViewlocationpage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	if(request.getParameter("id") == null){
		error("Es wurden nicht alle Variablen übergeben!", request, response);
	}
	int id = Integer.parseInt(request.getParameter("id"));
	LocationManagement lm = null;
	
	try{
		lm = new LocationManagement();
		Location mylocation = lm.getLocation(id);
		id = mylocation.getId();			
	}catch(Exception e){
		error("Eine Location mit dieser ID wurde nicht gefunden.", request, response);
	}
	request.setAttribute("id", id); 
		if(request.getParameter("rdatum") != null){
			request.setAttribute("rdatum", request.getParameter("rdatum"));
		}
	printpage("locationpage", request, response);	
}	
	/**
	 *
	 * Eventpage ausgeben
	 *
	 * 
	 */
	public void doVieweventpage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("id") == null){
			error("Es wurden nicht alle Variablen übergeben!", request, response);
		}
		int id = Integer.parseInt(request.getParameter("id"));
		EventManagement em = null;
		
		try{
			em = new EventManagement();
			Event myevent = em.getEvent(id);
			id = myevent.getId();			
		}catch(Exception e){
			error("Ein Event mit dieser ID wurde nicht gefunden.", request, response);
		}
		
		request.setAttribute("id", id); 
		printpage("eventpage", request, response);	
	}
	/**
	 *
	 * Eventsuche
	 *
	 * 
	 */
	public void doEventsearch(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("name") == null || request.getParameter("datum") == null){
			error("Sie müssen alle Daten angeben!", request, response);
		}

		request.setAttribute("name", request.getParameter("name"));
		request.setAttribute("datum", request.getParameter("datum"));
		printpage("eventsearch", request, response);
	}	
	/**
	 *
	 * Location fuer ein Event suchen/finden
	 *
	 * 
	 */
	public void doFindlocation(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("datum") == null || request.getParameter("name") == null || request.getParameter("typ") == null){
			error("Sie müssen alle Daten angeben!", request, response);
		}

		LocationManagement lm = null;
		ArrayList <Location> mylocations = null;
		
		String datum = request.getParameter("datum");
		
		if(!(datum.equals(""))){
			SimpleDateFormat sd = new SimpleDateFormat("dd.MM.yyyy");
			Date pdatum = null;
			try {
				pdatum = sd.parse(datum);
				datum = sd.format(pdatum);
			}
			catch (Exception e) 
			{}
		}
		
		try{
			lm = new LocationManagement();
			mylocations = lm.findAvailableLocations( datum, request.getParameter("name"), request.getParameter("typ") );
		}			
		catch(Exception e){			
			request.setAttribute("error", "FEHLER: " + e.getMessage());
			request.setAttribute("datum", datum);
			request.setAttribute("name", request.getParameter("name"));
			request.setAttribute("typ", request.getParameter("typ"));

			printpage("findlocation", request, response);	
		}
		
		request.setAttribute("datum", datum);
		request.setAttribute("name", request.getParameter("name"));
		request.setAttribute("typ", request.getParameter("typ"));
		request.setAttribute("locations", mylocations);
		request.setAttribute("locationcount", mylocations.size());
		printpage("findlocation", request, response);
	}	
	/**
	 *
	 * Neue Location anlegen
	 *
	 * 
	 */
	public void doCreatelocation(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("name") == null || request.getParameter("adresse") == null || request.getParameter("preis") == null || request.getParameter("typ") == null || request.getParameter("maxt") == null || request.getParameter("besch") == null){
			error("Sie müssen alle Daten angeben!", request, response);
		}

		LocationManagement lm = null;
		
		try{
			lm = new LocationManagement();
			lm.createLocation(request.getParameter("name"), Integer.valueOf(request.getParameter("maxt")), request.getParameter("typ"), request.getParameter("besch"), Double.valueOf(request.getParameter("preis")), request.getParameter("adresse"), (String) session.getAttribute("username"));

		} catch(NumberFormatException e) {
			request.setAttribute("error", "FEHLER: Preis und Kapazität müssen angegeben werden und eine Zahl sein!");
			
			request.setAttribute("name", request.getParameter("name"));
			request.setAttribute("maxt", request.getParameter("maxt"));
			request.setAttribute("typ", request.getParameter("typ"));
			request.setAttribute("besch", request.getParameter("besch"));
			request.setAttribute("preis", request.getParameter("preis"));
			request.setAttribute("adresse", request.getParameter("adresse"));

			printpage("createlocation", request, response);	
		}
		catch(Exception e){			
			request.setAttribute("error", "FEHLER: " + e.getMessage());
			
			request.setAttribute("name", request.getParameter("name"));
			request.setAttribute("maxt", request.getParameter("maxt"));
			request.setAttribute("typ", request.getParameter("typ"));
			request.setAttribute("besch", request.getParameter("besch"));
			request.setAttribute("preis", request.getParameter("preis"));
			request.setAttribute("adresse", request.getParameter("adresse"));

			printpage("createlocation", request, response);	
		}				

		response.sendRedirect("index?page=createlocation_success");
	}	
	/**
	 *
	 * Location löschen
	 *
	 * 
	 */
	public void doDeletelocation(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("id") == null){
			error("Es wurden nicht alle Daten übergeben!", request, response);
		}

		LocationManagement lm = null;
		
		try{
			lm = new LocationManagement();
			lm.deleteLocation(request.getParameter("id"));
		}
		catch(Exception e){			
			error(e.getMessage(), request, response);
		}				

		response.sendRedirect("index?page=delete_success");
	}	
	/**
	 *
	 * Neues Event anlegen
	 *
	 * 
	 */
	public void doCreateevent(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("create") == null){
			if(request.getParameter("id") == null || request.getParameter("date") == null){
				error("Interner Fehler! Es wurden nicht alle nötigen Variablen für diese Aktion übergeben!", request, response);
			}
			request.setAttribute("id", request.getParameter("id"));
			request.setAttribute("date", request.getParameter("date"));
			
			LocationManagement lm = new LocationManagement();
			Location mylocation = null;
			
			try{
				mylocation = lm.getLocation(Integer.valueOf(request.getParameter("id")));
			}catch(Exception e){
				error(e.getMessage(), request, response);
			}
			
			request.setAttribute("locationname", mylocation.getName());
			printpage("createevent", request, response);	
		}
		else{
			
			if(request.getParameter("name") == null || request.getParameter("id") == null || request.getParameter("datum") == null || request.getParameter("maxteilnehmer") == null || request.getParameter("preis") == null || request.getParameter("besch") == null || request.getParameter("uhrzeit") == null){
				error("Sie müssen alle Daten angeben!", request, response);
			}
			LocationManagement lm = new LocationManagement();
			Location mylocation = null;
			
			try{
				mylocation = lm.getLocation(Integer.valueOf(request.getParameter("id")));
			}catch(Exception e){
				error(e.getMessage(), request, response);
			}
	
			EventManagement em = null;
			
			try{
				em = new EventManagement();

				em.createEvent( request.getParameter("name"), Integer.valueOf(request.getParameter("id")), request.getParameter("datum"), request.getParameter("maxteilnehmer"),
						request.getParameter("preis"), request.getParameter("besch"), request.getParameter("uhrzeit"),
						(String) session.getAttribute("username") );			
			}
			catch(Exception e){			
				request.setAttribute("error", "FEHLER: " + e.getMessage());
				
				request.setAttribute("name", request.getParameter("name"));
				request.setAttribute("id", request.getParameter("id"));
				request.setAttribute("date", request.getParameter("datum"));
				request.setAttribute("datum", request.getParameter("datum"));
				request.setAttribute("maxteilnehmer", request.getParameter("maxteilnehmer"));
				request.setAttribute("preis", request.getParameter("preis"));
				request.setAttribute("besch", request.getParameter("besch"));
				request.setAttribute("uhrzeit", request.getParameter("uhrzeit"));
				request.setAttribute("locationname", mylocation.getName());
				
				printpage("createevent", request, response);	
			}				

			response.sendRedirect("index?page=createevent_success");
		
		}
	}	
	/**
	 *
	 * Event löschen
	 *
	 * 
	 */
	public void doDeleteevent(HttpSession session, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("id") == null){
			error("Es wurden nicht alle Daten übergeben!", request, response);
		}

		EventManagement em = null;
		
		try{
			em = new EventManagement();
			em.deleteEvent(request.getParameter("id"));
		}
		catch(Exception e){			
			error(e.getMessage(), request, response);
		}				

		response.sendRedirect("index?page=delete_success");
	}	
	/**
	 *
	 * Error Ausgabe, wenn Error in einer JSP auftritt
	 *
	 * 
	 */
	public void doError(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("code") != null){
			String code = request.getParameter("code");
			if(code.equals("nopermission")) error("Sie haben keine Berechtigung diese Seite aufzurufen! Sind Sie eventuell nicht eingeloggt?", request, response);
			if(code.equals("eventnotfound")) error("Ein Event mit dieser ID wurde nicht gefunden.", request, response);
			if(code.equals("locationnotfound")) error("Eine Location mit dieser ID wurde nicht gefunden.", request, response);
		}	
		error("Es ist ein Fehler aufgetreten.", request, response);
	}	
	/**
	 *
	 * Registrierung durchführen
	 *
	 * 
	 */
	public void doSignup(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {			
		if(request.getParameter("username") == null || request.getParameter("vorname") == null || request.getParameter("nachname") == null || request.getParameter("email") == null || request.getParameter("gender") == null || request.getParameter("password") == null || request.getParameter("day") == null || request.getParameter("month") == null || request.getParameter("year") == null || request.getParameter("rolle") == null || request.getParameter("pwcheck") == null){
			error("Sie müssen alle Felder ausfüllen!", request, response);
		}
		try{
			UserManagement um = new UserManagement();

			um.createUser(request.getParameter("username"), request.getParameter("vorname"), request.getParameter("nachname"), 
					request.getParameter("email"), request.getParameter("gender"), request.getParameter("password"), 
					request.getParameter("day") + "." + request.getParameter("month") + "." + request.getParameter("year"), request.getParameter("rolle"), request.getParameter("pwcheck"));
		} catch(Exception e){
			// error(e.getMessage(), request, response);
			
			request.setAttribute("error", "FEHLER: " + e.getMessage());
			request.setAttribute("username", request.getParameter("username"));
			request.setAttribute("vorname", request.getParameter("vorname"));
			request.setAttribute("nachname", request.getParameter("nachname"));
			request.setAttribute("email", request.getParameter("email"));
			request.setAttribute("gender", request.getParameter("gender"));
			request.setAttribute("day", request.getParameter("day"));
			request.setAttribute("month", request.getParameter("month"));
			request.setAttribute("year", request.getParameter("year"));
			request.setAttribute("rolle", request.getParameter("rolle"));
			printpage("signup", request, response);
		}				
		response.sendRedirect("index?page=signup_success");
	}
		
	
	/**
	 *
	 * Ausgabe eines Templates
	 *
	 * @param pagename Bekommt den Pagename als String uebergeben
	 * 
	 */
	public void printpage(String pagename, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {					
			JSPPage = getServletContext().getRequestDispatcher("/templates/" + pagename + ".jsp");
			JSPPage.forward(request, response);	
	}
		
		/**
		 *
		 * Ausgabe eines Error-Templates
		 *
		 * @param errormeldung Bekommt die Errormeldung zur Ausgabe als Srting uebergeben
		 * 
		 */
		public void error(String errormeldung, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{	
			
			JSPPage = getServletContext().getRequestDispatcher("/templates/error.jsp");
			request.setAttribute("error", errormeldung);
			JSPPage.forward(request, response);	
		}
		
		/**
		 *
		 * Ausgabe eines Error-Templates
		 *
		 * @param errormeldung Bekommt die Errormeldung zur Ausgabe als Srting uebergeben
		 * 
		 */
		public void notification(String meldung, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{	
			
			JSPPage = getServletContext().getRequestDispatcher("/templates/notification.jsp");
			request.setAttribute("meldung", meldung);
			JSPPage.forward(request, response);	
		}
		

	/**
	 *
	 * Ueberprueft ob Page existiert, gibt true oder false zurueck
	 *
	 * @param pagename Bekommt den Pagename als String uebergeben

	 * 
	 */	
	public boolean doesPageExist(String pagename){
		
		boolean page_exists = false;
		
		for(int i = 0; i < pages.length; i++){
			if(pagename.toLowerCase().equals(pages[i])){
				page_exists = true;
			}
		}
		return page_exists;
	}

	public void init() throws ServletException
	{
		// Ich putze hier nur.
	}

	  public void destroy()
	  {
		// Ich putze hier ebenfalls nur.
	  }

	/**
	 *
	 * Abfangen von Get-Anfragen
	 *
	 * @param request Http Servlet Request
	 * @param response Http Servlet Response
	 * 
	 */
		public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		try{
			doRequest(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 *
	 * Abfangen von Post-Anfragen
	 *
	 * @param request Http Servlet Request
	 * @param response Http Servlet Response
	 * 
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException
	{
		try{
			doRequest(request, response);
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
