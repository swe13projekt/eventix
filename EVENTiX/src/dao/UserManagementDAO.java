/**
 * Diese Klasse ist zuständig für das persistente speichern, laden und aktualisieren der Benutzer.
 * Die persistente Speicherung erfolgt über Serialisierung der Benutzer die sich in einer ArrayList befinden.
 * @author Andreas Fuchs
 * @author Dominic Boehm
 */
package dao;

import users.RegUser;

import java.io.*;
import java.lang.IllegalArgumentException;
import java.util.ArrayList;


public class UserManagementDAO implements IUserManagementDAO 
{

//Classvariable
	private static String filename = "users.ser";

//Constructor
	public UserManagementDAO() throws IOException
	{
		createFile( UserManagementDAO.filename );
	}
	
//Getter and Setter
	public static String getFilename()
	{
		return filename;
	}

	public static void setFilename( String filename )
	{
		UserManagementDAO.filename = filename;
	}

//Object Methods
	@SuppressWarnings("unchecked")
	/** 
	 * @see dao.IUserManagementDAO#getUserList()
	 * Liest alle User aus der Datei und gibt diese als ArrayList zurück.
	 * Im Falle dessen das die Datei nicht existiert wird eine neue Datei angelegt und null als Wert zurückgegeben.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen der Datei kommt.
	 */
	@Override
	public ArrayList<RegUser> getUserList() throws IOException
	{
		 ArrayList<RegUser> userList = new ArrayList<RegUser>();
		    
		 FileInputStream fis = null;
		 ObjectInputStream ois = null;
		 try
		 {
			 
			 fis = new FileInputStream( UserManagementDAO.filename );
			 
			 if( fis.available() > 0 )
			 {
				 ois = new ObjectInputStream( fis );
			     userList = ( ArrayList<RegUser> )ois.readObject();
			 }
		 } catch( FileNotFoundException e )
		 {
			 createFile( UserManagementDAO.filename );
			 return userList;
		 } catch( ClassNotFoundException e )
		 {
			 return userList;
		 } catch( NullPointerException e )
		 {
			 return userList;
		 }
		 finally
		 {
			 try
			 {
				 ois.close();
				 fis.close();
			 } catch( NullPointerException e )
			 {
				 return userList;
			 }
		 }
		 
		 return userList;
	}

	/** 
	 * @see dao.IUserManagementDAO#updateUser(users.RegUser)
	 * Aktualisiert die Informationen zu einem bestimmten Benutzer.
	 * Falls der Benutzer nicht gefunden werden kann wird eine IllegalArgumentException geworfen.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void updateUser( RegUser regUser ) throws IllegalArgumentException, IOException 
	{
		ArrayList<RegUser> userList = getUserList();
		
		for( int i = 0; i < userList.size(); ++i )
		{
			if( userList.get( i ).getUsername().equals(regUser.getUsername() ) )
			{
				userList.set( i, regUser );
				saveData( userList );		
				return;
			}
		}
		
		throw new IllegalArgumentException( "Der Benutzer konnte nicht gefunden werden. Aktualisieren fehlgeschlagen." );
	}

	/** 
	 * @see dao.IUserManagementDAO#getUserbyUsername(java.lang.String)
	 * Sucht einen bestimmten Benutzer identifiziert durch den Benutzernamen und gibt diesen zurück.
	 * Falls kein Benutzer gefunden wird, wird eine IllegalArgumentException geworfen.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public RegUser getUserbyUsername( String username ) throws IllegalArgumentException, IOException
	{
		ArrayList<RegUser> userList = getUserList();
		
		for( int i = 0; i < userList.size(); ++i )
		{
			RegUser user = userList.get( i );
			if( user.getUsername().equalsIgnoreCase( username ) )
				return user;
		}
		
		throw new IllegalArgumentException( "Der Benutzer konnte nicht gefunden werden." ); 
	}

	/** 
	 * @see dao.IUserManagementDAO#deleteUser(java.lang.String)
	 * Löscht den Benutzer identifiziert durch den Benutzername.
	 * Falls der Benutzer nicht existiert wird einen IllegalArgumentException geworfen.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void deleteUser( String username ) throws IllegalArgumentException, IOException
	{
		ArrayList<RegUser> userList = getUserList();
		
		for( int i = 0; i < userList.size(); ++i)
		{
			if( userList.get( i ).getUsername().equals( username ) )
			{
				userList.remove( i );
				saveData( userList );
				return;
			}
		}
				
		throw new IllegalArgumentException( "Der angegebene Benutzername existiert nicht." );
	}

	/**
	 * @see dao.IUserManagementDAO#addUser(users.RegUser)
	 * Fügt einen neuen Benutzer hinzu.
	 * Überprüft zunächst ob der Username oder die Email-Adresse des übergebenen Benutzers bereits benutzt wird.
	 * Wenn dies der Fall ist wird eine IllegalArgumentException geworfen.
	 * Wenn dies nicht der Fall ist wird der neue Benutzer zur Liste hinzugefügt und persistent gespeichert.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void addUser( RegUser regUser ) throws IllegalArgumentException, IOException
	{	
		ArrayList<RegUser> userList = getUserList();
		
		for( int i = 0; i < userList.size(); ++i )
		{
			RegUser user = userList.get( i );
			if( user.getUsername().equals( regUser.getUsername() ) )
				throw new IllegalArgumentException( "Der Benutzername wird bereits verwendet. Bitte wählen Sie einen anderen." );
			else if( user.geteMail().equals( regUser.geteMail() ) )
				throw new IllegalArgumentException( "Diese Email-Adresse wird bereits verwendet." );
		}
				
		userList.add( regUser );
		
		saveData( userList );
	}
	
	/**
	 * @see dao.IUserManagementDAO#saveData(java.util.ArrayList)
	 * Speichert die übergebene ArrayList persistent in eine Datei.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void saveData( ArrayList<RegUser> arr_users ) throws IOException
	{
		//Eine neue Datei anlegen (falls diese noch nicht existiert)
		ObjectOutputStream oos = null;
		FileOutputStream fos = null;
		
		createFile( UserManagementDAO.filename );
		
		try{
			fos = new FileOutputStream( UserManagementDAO.filename );
			oos = new ObjectOutputStream( fos );
			
			oos.writeObject( arr_users );
		} finally
		{
			try
			{
				oos.close();
				fos.close();
			} catch (NullPointerException e){}
		}		
	}
	
	/**
	 * Löscht die Datei und erzeugt eine neue leere Datei.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	public void flushFile( String filename ) throws IOException
	{
		File target = new File( filename );
		if( target.delete() )
			createFile( filename );
	}
	
	/**
	 * Erzeugt eine neue Datei falls diese noch nicht existiert.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	private void createFile( String filename ) throws IOException
	{
		File target = new File( filename );
		
		// erzeuge eine neue Datei (nur dann wenn diese noch nicht existiert)
		target.createNewFile();
	}
}
