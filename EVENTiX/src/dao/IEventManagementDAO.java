/**
 * Diese Klasse ist zuständig für das persistente speichern, laden und aktualisieren der Locations. 
 * Die persistente Speicherung erfolgt über Serialisierung der Locations die sich in einer ArrayList befinden.
 * @author Andreas Fuchs
 * @author Dominic Boehm
 */

package dao;

import java.io.IOException;
import java.util.ArrayList;
import events.Event;

public interface IEventManagementDAO 
{
	public ArrayList<Event> getEventList() throws IOException;
	public void updateEvent(Event event) throws IllegalArgumentException, IOException;
	public void deleteEvent(String id) throws IllegalArgumentException, IOException;
	public void addEvent(Event event) throws IllegalArgumentException, IOException;
	public void saveData(ArrayList<Event> arr_events) throws IOException;
	public Event getEventbyID(int id) throws IllegalArgumentException, IOException;
}
