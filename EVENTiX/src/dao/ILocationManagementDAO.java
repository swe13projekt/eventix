/**
 * @author Dominic Boehm
 * @author Andreas Fuchs
 * Interface zur persistenten Speicherung der Locations
 */

package dao;

import java.io.IOException;
import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import locations.Location;

public interface ILocationManagementDAO 
{
	public ArrayList<Location> getLocationList() throws IOException;
	public void updateLocation(Location location) throws IllegalArgumentException, IOException;
	public void deleteLocation(String id) throws IllegalArgumentException, IOException;
	public void addLocation(Location location) throws IllegalArgumentException, IOException;
	public void saveData(ArrayList<Location> arr_locations) throws IOException;
	public Location getLocationbyID(int id) throws IllegalArgumentException, IOException;
}
