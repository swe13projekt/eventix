/**
 * Diese Klasse ist zuständig für das persistente speichern, laden und aktualisieren der Veranstaltungen. 
 * Die persistente Speicherung erfolgt über Serialisierung der Veranstaltungen die sich in einer ArrayList befinden.
 * @author Andreas Fuchs
 * @author Dominic Boehm
 */

package dao;

import java.io.*;
import java.util.ArrayList;

import events.Event;

public class EventManagementDAO implements IEventManagementDAO 
{
//Classvariable
	private static String filename = "events.ser";
	
//Constructor
	public EventManagementDAO() throws IOException
	{
		createFile( EventManagementDAO.filename );
	}

//Object Methods
	
	@SuppressWarnings("unchecked")
	/** 
	 * @see dao.IEventManagementDAO#getEventList()
	 * Liest alle Veranstaltungen aus der Datei und gibt diese als ArrayList zurück.
	 * Im Falle dessen dass die Datei nicht existiert wird eine neue Datei angelegt und null als Wert zurückgegeben.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen der Datei kommt.
	 */
	@Override
	public ArrayList<Event> getEventList() throws IOException
	{
		ArrayList<Event> eventList = new ArrayList<Event>();
	    
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try
		{
			fis = new FileInputStream( EventManagementDAO.filename );
			
			if( fis.available() > 0 )
			{
				ois = new ObjectInputStream( fis );
			    eventList = ( ArrayList<Event> )ois.readObject();
			}
		} catch( FileNotFoundException e )
		{
			createFile( EventManagementDAO.filename );
			return eventList;
		} catch( ClassNotFoundException e )
		{
			return eventList;
		} finally
		{
			try
			 {
				 ois.close();
				 fis.close();
			 } catch( NullPointerException e )
			 {
				 return eventList;
			 }
		}
		
		return eventList;	
	}

	/** 
	 * @see dao.IEventManagementDAO#updateEvent(events.Event)
	 * Aktualisiert die Informationen zu einer bestimmten Veranstaltung.
	 * Falls die Veranstaltung nicht gefunden werden kann wird eine IllegalArgumentException geworfen.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void updateEvent(Event event) throws IllegalArgumentException, IOException
	{
		ArrayList<Event> eventList = getEventList();

		for( int i = 0; i < eventList.size(); ++i )
		{
			if( eventList.get( i ).getId() == event.getId() )
			{
				eventList.set( i, event );
				saveData( eventList );
				return;
			}
		}
				
		throw new IllegalArgumentException( "Das Event konnte nicht gefunden werden. Aktualisieren fehlgeschlagen." );
	}

	/** 
	 * @see dao.IEventManagementDAO#deleteEvent(java.lang.String)
	 * Löscht die Veranstaltung identifiziert durch die übergebene id.
	 * Falls die Veranstaltung nicht existiert oder die übergebene id keine Zahl ist wird einen IllegalArgumentException geworfen.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void deleteEvent( String id ) throws IllegalArgumentException, IOException
	{
		int idNo;
		try
		{
			idNo = Integer.parseInt( id );
		} catch( NumberFormatException e )
		{
			throw new IllegalArgumentException( "Die eingegebene ID ist keine Nummer." );
		}
		
		ArrayList<Event> eventList = getEventList();

		for( int i = 0; i < eventList.size(); ++i)
			{
				if( eventList.get( i ).getId() == idNo )
				{
					eventList.remove( i );
					saveData( eventList );
					return;
				}
			}
			
		throw new IllegalArgumentException( "Die Veranstaltung wurde nicht gefunden und konnte nicht gelöscht werden." );	
	}

	/**
	 * @see dao.IEventManagementDAO#addEvent(events.Event)
	 * Fügt einen neue Veranstaltung hinzu.
	 * Überprüft zunächst ob die ID der hinzufügenden Veranstaltung bereits verwendet wird.
	 * Wenn dies der Fall ist wird eine IllegalArgumentException geworfen.
	 * Wenn dies nicht der Fall ist wird die neue Location zur Liste hinzugefügt und persistent gespeichert.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void addEvent(Event event) throws IllegalArgumentException, IOException
	{
		ArrayList<Event> eventList = getEventList();
		
		for( int i = 0; i < eventList.size(); ++i )
		{
			Event eve = eventList.get( i );
			if( eve.getId() == event.getId() )
				throw new IllegalArgumentException( "Die EventID wird bereits verwendet." );
		}
				
		eventList.add( event );
		
		saveData( eventList );
	}
	
	/**
	 * @see dao.IEventManagementDAO#saveData(java.util.ArrayList)
	 * Speichert die übergebene ArrayList persistent in eine Datei.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void saveData(ArrayList<Event> arr_events) throws IOException
	{
		ObjectOutputStream oos = null;
		FileOutputStream fos = null;
		
		//Eine neue Datei anlegen (falls diese noch nicht existiert)
		createFile( EventManagementDAO.filename );
		
		try{
			fos = new FileOutputStream( EventManagementDAO.filename );
			oos = new ObjectOutputStream( fos );
			
			oos.writeObject( arr_events );
		} finally
		{
			try
			{
				oos.close();
				fos.close();
			} catch( NullPointerException e ){}
		}		
	}

	/** 
	 * @see dao.IEventManagementDAO#getEventbyID(int)
	 * Sucht eine bestimmte Veranstaltung identifiziert durch ihre ID und gibt diese zurück.
	 * Falls keine Veranstaltung gefunden wird, wird eine IllegalArgunentException geworfen.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public Event getEventbyID(int id) throws IllegalArgumentException, IOException
	{
		ArrayList<Event> eventList = getEventList();
		
		for( int i = 0; i < eventList.size(); ++i )
		{
			Event event = eventList.get( i );
			if( event.getId() == id )
				return event;
		}
		throw new IllegalArgumentException( "Event konnte nicht gefunden werden." );

	}
	
	/**
	 * Löscht die Datei und erzeugt eine neue leere Datei.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	public void flushFile( String filename ) throws IOException
	{
		File target = new File( filename );
		if( target.delete() )
			createFile( filename );
	}
	
	/**
	 * Erzeugt eine neue Datei falls diese noch nicht existiert.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	private void createFile( String filename ) throws IOException
	{
		File target = new File( filename );
		
		// erzeuge eine Neue Datei (nur dann wenn diese noch nicht existiert)
		target.createNewFile();
	}

}
