/**
 * @author Dominic Boehm
 * @author Andreas Fuchs
 * Interface zur persistenten Speicherung der User
 */

package dao;

import java.io.IOException;
import java.lang.IllegalArgumentException;
import java.util.ArrayList;
import users.RegUser;

public interface IUserManagementDAO 
{
	public ArrayList<RegUser> getUserList() throws IOException;
	public void updateUser(RegUser regUser) throws IllegalArgumentException, IOException;
	public RegUser  getUserbyUsername(String username) throws IllegalArgumentException, IOException;
	public void deleteUser(String username) throws IllegalArgumentException, IOException;
	public void addUser(RegUser regUser) throws IllegalArgumentException, IOException;
	public void saveData(ArrayList<RegUser> arr_users) throws IOException;
}
