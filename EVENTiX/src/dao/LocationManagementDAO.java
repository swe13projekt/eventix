/**
 * Diese Klasse ist zuständig für das persistente speichern, laden und aktualisieren der Locations. 
 * Die persistente Speicherung erfolgt über Serialisierung der Locations die sich in einer ArrayList befinden.
 * @author Andreas Fuchs
 * @author Dominic Boehm
 */

package dao;

import java.io.*;
import java.util.ArrayList;
import java.lang.IllegalArgumentException;

import locations.Location;

public class LocationManagementDAO implements ILocationManagementDAO
{

//Classvariable
	private static String filename = "locations.ser";
	
//Constructor	
	public LocationManagementDAO() throws IOException
	{
		createFile( LocationManagementDAO.filename );
	}

//Object Methods
	
	/** 
	 * @see dao.ILocationManagementDAO#getLocationList()
	 * Liest alle Locations aus der Datei und gibt diese als ArrayList zurück.
	 * Im Falle dessen dass die Datei nicht existiert wird eine neue Datei angelegt und null als Wert zurückgegeben.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen der Datei kommt.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ArrayList<Location> getLocationList() throws IOException
	{
		ArrayList<Location> locationList = new ArrayList<Location>();
	    
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try
		{
			fis = new FileInputStream( LocationManagementDAO.filename );
			
			if( fis.available() > 0 )
			{
				ois = new ObjectInputStream( fis );
			    locationList = ( ArrayList<Location> )ois.readObject();
			}
		} catch( FileNotFoundException e )
		{
			createFile( LocationManagementDAO.filename );
			return locationList;
		} catch( ClassNotFoundException e )
		{
			return locationList;
		} finally
		{
			try
			 {
				 ois.close();
				 fis.close();
			 } catch( NullPointerException e)
			 {
				 return locationList;
			 }
		}
		
		return locationList;
	}

	/** 
	 * @see dao.ILocationManagement#updateLocation(locations.Location)
	 * Aktualisiert die Informationen zu einer bestimmten Location.
	 * Falls die Location nicht gefunden werden kann wird eine IllegalArgumentException geworfen.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void updateLocation( Location location ) throws IllegalArgumentException, IOException
	{
		ArrayList<Location> locationList = getLocationList();

		for( int i = 0; i < locationList.size(); ++i )
		{
			if( locationList.get( i ).getId() == location.getId() )
			{
				locationList.set( i, location );
				saveData( locationList );
				return;
			}
		}
				
		throw new IllegalArgumentException( "Die Location konnte nicht gefunden werden. Aktualisieren fehlgeschlagen" );
	}

	/** 
	 * @see dao.ILocationManagementDAO#deleteLocation(java.lang.String)
	 * Löscht die Location identifiziert durch die übergebene id.
	 * Falls die Location nicht existiert oder die übergebene id keine Zahl ist wird einen IllegalArgumentException geworfen.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void deleteLocation( String id ) throws IllegalArgumentException, IOException
	{
		int idNo;
		try
		{
			idNo = Integer.parseInt( id );
		} catch( NumberFormatException e )
		{
			throw new IllegalArgumentException( "Die eingegebene ID ist keine Nummer." );
		}
		
		ArrayList<Location> locationList = getLocationList();
		
		for( int i = 0; i < locationList.size(); ++i)
		{
			if( locationList.get( i ).getId() == idNo )
			{
				locationList.remove( i );
				saveData( locationList );
				return;
			}
		}
		
		throw new IllegalArgumentException( "Die Location wurde nicht gefunden und konnte nicht gelöscht werden." );	
	}

	/**
	 * @see dao.ILocationManagementDAO#addLocation(locations.Location)
	 * Fügt einen neue Location hinzu.
	 * Überprüft zunächst ob die ID der hinzufügenden Location bereits verwendet wird.
	 * Wenn dies der Fall ist wird eine IllegalArgumentException geworfen.
	 * Wenn dies nicht der Fall ist wird die neue Location zur Liste hinzugefügt und persistent gespeichert.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void addLocation(Location location) throws IllegalArgumentException, IOException
	{
		ArrayList<Location> locationList = getLocationList();
		
		for( int i = 0; i < locationList.size(); ++i )
		{
			Location loc = locationList.get( i );
			if( loc.getId() == location.getId() )
				throw new IllegalArgumentException( "Die LocationID wird bereits verwendet." );
		}
		
		locationList.add( location );
		
		saveData( locationList );
	}

	/**
	 * @see dao.ILocationManagementDAO#saveData(java.util.ArrayList)
	 * Speichert die übergebene ArrayList persistent in eine Datei.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public void saveData( ArrayList<Location> arr_locations ) throws IOException
	{
		
		ObjectOutputStream oos = null;
		FileOutputStream fos = null;
		
		//Eine neue Datei anlegen (falls diese noch nicht existiert)
		createFile( LocationManagementDAO.filename );
		
		try{
			fos = new FileOutputStream( LocationManagementDAO.filename );
			oos = new ObjectOutputStream( fos );
			
			oos.writeObject( arr_locations );
		} finally
		{
			try
			{
				oos.close();
				fos.close();
			} catch( NullPointerException e ) {}
		}		
	}

	/** 
	 * @see dao.ILocationManagementDAO#getLocationbyID(int)
	 * Sucht eine bestimmte Location identifiziert durch ihre ID und gibt diese zurück.
	 * Falls keine Location gefunden wird, wird eine IllegalArgumentException geworfen.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	@Override
	public Location getLocationbyID(int id) throws IllegalArgumentException, IOException
	{
		ArrayList<Location> locationList = getLocationList();
		
		for( int i = 0; i < locationList.size(); ++i )
		{
			Location location = locationList.get( i );
			if( location.getId() == id )
				return location;
		}
				
		throw new IllegalArgumentException( "Location konnte nicht gefunden werden." );
	}
	
	/**
	 * Löscht die Datei und erzeugt eine neue leere Datei.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	public void flushFile( String filename ) throws IOException
	{
		File target = new File( filename );
		if( target.delete() )
			createFile( filename );
	}	
	
	/**
	 * Erzeugt eine neue Datei falls diese noch nicht existiert.
	 * Wirft eine IOException falls es zu Problemen bei dem Lesen oder Schreiben der Datei kommt.
	 */
	private void createFile( String filename ) throws IOException
	{
		File target = new File( filename );
		
		// erzeuge eine Neue Datei (nur dann wenn diese noch nicht existiert)
		target.createNewFile();
	}

}
