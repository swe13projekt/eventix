/**
 * 
 */
package users;

import java.util.Date;

/**
 * @author dominic
 *
 */
public class Analyst extends RegUser{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String organisation;
	
	
	public Analyst(String username, String vorname, String nachname,
			String eMail, char geschlecht, String password, Date regDate,
			Date gebDate, String organisation) {
		super(username, vorname, nachname, eMail, geschlecht, password, regDate,
				gebDate);
		setOrganisation(organisation);
	}

	/**
	 * 
	 */

	public String getOrganisation() {
		return htmlfilter(organisation);
	}
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
	
	private String htmlfilter(String text) {
		text.replaceAll("<","&lt;");
		text.replaceAll(">","&gt;");
		return text;
	}
}
