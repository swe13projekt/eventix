package users;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class RegUser implements Serializable{

	
	private String username;
	private String vorname;
	private String nachname;
	private String eMail;
	private char geschlecht;
	private String password;
	private Date regDate;
	private Date gebDate;
	
	
	/**
	 * @param username
	 * @param vorname
	 * @param nachname
	 * @param eMail
	 * @param geschlecht
	 * @param password
	 * @param regDate
	 * @param gebDate
	 */
	public RegUser(String username, String vorname, String nachname,
			String eMail, char geschlecht, String password, Date regDate,
			Date gebDate) {
						
		setUsername(username);
		setVorname(vorname);
		setNachname(nachname);
		seteMail(eMail);
		setGeschlecht(geschlecht);
		setPassword(password);
		setRegDate(regDate);
		setGebDate(gebDate);
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * Prüft ob username keine Sonderzeichen enthält.
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		if(username == "" || username == null )
			throw new IllegalArgumentException("Es muss ein Username angegeben werden!");
		else{
			if(username.matches("[\\w]*") && username.length() >= 4 && !filter(username)){
				this.username = username;
			}else
				throw new IllegalArgumentException("Bitte wählen Sie einen Username, der keine Sonderzeichen enthält und mindestens 4 Zeichen lang ist!");
		}
	}
	/**
	 * @return the vorname
	 */
	public String getVorname() {
		return htmlfilter(vorname);
	}
	/**
	 * @param vorname the vorname to set
	 */
	public void setVorname(String vorname) {
		if(vorname == "" || vorname == null)
			throw new IllegalArgumentException("Sie müssen Ihren Vornamen anegeben!");
		else{
			if(!vorname.matches(".*[0-9].*") && vorname.length() >= 3 && !filter(vorname)){
				this.vorname = vorname;
			}else
				throw new IllegalArgumentException("Im Vornamen sind keine Zahlen und Sonderzeichen erlaubt. Desweiteren muss dieser mind. 3 Zeichen haben.");
		}
	}
	/**
	 * @return the nachname
	 */
	public String getNachname() {
		return htmlfilter(nachname);
	}
	/**
	 * @param nachname the nachname to set
	 */
	public void setNachname(String nachname) {
		if(nachname == "" || nachname == null)
			throw new IllegalArgumentException("Sie müssen Ihren Nachnamen angeben!");
		else{
			if(!nachname.matches(".*[0-9].*") && !filter(nachname) && nachname.length() >= 3){
				this.nachname = nachname;
			}else
				throw new IllegalArgumentException("Im Nachnamen sind keine Zahlen und Sonderzeichen erlaubt. Desweiteren muss dieser mind. 3 Zeichen haben.");
		}

	}
	/**
	 * @return the eMail
	 */
	public String geteMail() {
		return htmlfilter(eMail);
	}
	/**
	 * @param eMail the eMail to set
	 */
	public void seteMail(String eMail) {
		if(eMail == "" || eMail == null)
			throw new IllegalArgumentException("Sie müssen Ihre Email-Adresse angeben!");
		else{
			if(eMail.contains("@") && eMail.contains(".") && eMail.length() >= 5){
				if(!eMail.contains(" ")){
					this.eMail = eMail;
				}else
					throw new IllegalArgumentException("Bitte geben Sie eine korrekte Email-Adresse an!");
			}else
				throw new IllegalArgumentException("Bitte geben Sie eine korrekte Email-Adresse an!");
		}
		
	}
	/**
	 * @return the geschlecht
	 */
	public char getGeschlecht() {
		return geschlecht;
	}
	/**
	 * @param geschlecht the geschlecht to set
	 */
	public void setGeschlecht(char geschlecht) {
		if(geschlecht == 'M' || geschlecht == 'm'){
			this.geschlecht = 'm';
		}else if(geschlecht == 'W' || geschlecht == 'w'){
			this.geschlecht = 'w';
		}else{
			throw new IllegalArgumentException("Das Geschlecht muss M oder W sein!");
		}
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * Prueft beim Eintragen des Passworts, ob die Laenge der 
	 * Eingabe > 6 ist.
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		if(password == "" || password == null)
			throw new IllegalArgumentException("Es muss ein Passwort angegeben werden!");
		else{
			if(password.length() >= 6) {
				this.password = password;
			}else
				throw new IllegalArgumentException("Das Passwort muss mindestens 6 Zeichen lang sein!");
		}
	}
	/**
	 * @return the regDate
	 */
	public Date getRegDate() {
		return regDate;
	}
	/**
	 * @param regDate the regDate to set
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	/**
	 * @return the gebDate
	 */
	public Date getGebDate() {
		return gebDate;
	}
	/**
	 * @param gebDate the gebDate to set
	 */
	public void setGebDate(Date gebDate) {
		Date today = new Date();
		
		if(gebDate == null)
			throw new IllegalArgumentException("Das Geburtsdatum muss angegeben werden!");
		else{
			if(gebDate.before(today)){
				this.gebDate = gebDate;
			}else{
				throw new IllegalArgumentException("Das Geburtsdatum darf nicht in der Zukunft liegen!");
			}
		}
	}
	

	public int getUserStatus() {
		
		if(this instanceof Analyst)
			return 3;
		else if(this instanceof Vermieter)
			return 2;
		else if(this instanceof Veranstalter)
			return 1;
		else
			return 0;
	}
	
	public String getFullName(){
		return vorname + " " + nachname;
	}
	
	private String htmlfilter(String text) {
		text.replaceAll("<","&lt;");
		text.replaceAll(">","&gt;");
		return text;
	}
	
	private boolean filter(String text) {
		if(text.contains("<") || text.contains(">") || text.contains("!") || text.contains("?") || text.contains("'") || text.contains("\"") || text.contains("#")
				|| text.contains("(") || text.contains(")") || text.contains("[") || text.contains("]") || text.contains("/") || text.contains("\\") || text.contains("€")
				|| text.contains("{") || text.contains("}") || text.contains("&") || text.contains("%") || text.contains("$") || text.contains("§") || text.contains("~")
				|| text.contains("*") || text.contains("+"))
			return true;
		return false;
	}
}
