package users;

import java.util.Date;

public class Vermieter extends RegUser{


		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private String organisation;
		private String adresse;
		private String telNummer;
		
		
		public Vermieter(String username, String vorname, String nachname,
				String eMail, char geschlecht, String password, Date regDate,
				Date gebDate, String organisation, String adresse, String telNummer) {
			super(username, vorname, nachname, eMail, geschlecht, password, regDate,
					gebDate);
			setOrganisation(organisation);
			setAdresse(adresse);
			setTelNummer(telNummer);
		}

		
		/**
		 * @return the organisation
		 */
		public String getOrganisation() {
			return htmlfilter(organisation);
		}
		/**
		 * @param organisation the organisation to set
		 */
		public void setOrganisation(String organisation) {
			this.organisation = organisation;
		}
		/**
		 * @return the adresse
		 */
		public String getAdresse() {
			return htmlfilter(adresse);
		}
		/**
		 * @param adresse the adresse to set
		 */
		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		/**
		 * @return the telNummer
		 */
		public String getTelNummer() {
			return htmlfilter(telNummer);
		}
		/**
		 * @param telNummer the telNummer to set
		 */
		public void setTelNummer(String telNummer) {
			this.telNummer = telNummer;
		}
		
		private String htmlfilter(String text) {
			text.replaceAll("<","&lt;");
			text.replaceAll(">","&gt;");
			return text;
		}
}
