package events;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import locations.Location;
import users.RegUser;
import dao.LocationManagementDAO;
import dao.UserManagementDAO;


@SuppressWarnings("serial")
/**
 * 
 * @author Dominic Boehm
 * @author James Patrick Salazar
 * Klasse zur Datenhaltung der Events
 * 
 */
public class Event implements Comparable<Event>, Serializable{
	
	private int id;
	private String name;
	private Date datum;
	private int location_id;
	private int maxTeilnehmer;
	private double preis;
	private String beschreibung;
	private String beginnzeit;
	private ArrayList<RegUser> teilnehmer;
	private String veranstalter;

	
	
	
	/**
	 * Konstruktor zum initialisieren aller Attribute.
	 * @param id
	 * @param name
	 * @param location_id
	 * @param maxTeilnehmer
	 * @param preis
	 * @param beschreibung
	 * @param beginnzeit
	 * @param teilnehmer
	 */
	public Event(int id, String name, int location_id, Date datum, int maxTeilnehmer,
			double preis, String beschreibung, String beginnzeit,
			ArrayList<RegUser> teilnehmer, String veranstalter) {
		
		teilnehmer = new ArrayList<RegUser>();
		
		setId(id);
		setName(name);
		setLocation_id(location_id);
		setMaxTeilnehmer(maxTeilnehmer);
		setPreis(preis);
		setBeschreibung(beschreibung);
		setBeginnzeit(beginnzeit);
		setTeilnehmer(teilnehmer);
		setDatum(datum);
		setVeranstalter(veranstalter);
	
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return htmlfilter(name);
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		if(name == "" || name == null || filter(name))
			throw new IllegalArgumentException("Es muss ein gültiger Eventname angegeben werden, es sind keine Sonderzeichen erlaubt!");
		else{
			this.name = name;
		}
	}
	/**
	 * @return the location_id
	 */
	public int getLocation_id() {
		return location_id;
	}
	/**
	 * @param location_id the location_id to set
	 */
	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}
	/**
	 * @return the maxTeilnehmer
	 */
	public int getMaxTeilnehmer() {
		return maxTeilnehmer;
	}
	/**
	 * @param maxTeilnehmer the maxTeilnehmer to set
	 * @throws IllegalArgumentException wenn maxTeilnehmer <= 0
	 */
	public void setMaxTeilnehmer(int maxTeilnehmer) {
		if(maxTeilnehmer >= 0)
			this.maxTeilnehmer = maxTeilnehmer;
		else
			throw new IllegalArgumentException("Es muss eine max. Teilnehmeranzahl angegeben werden!");
	}
	/**
	 * @return the preis auf zwei Nachkommerstellen gerundet
	 */
	public double getPreis() {
		return (Math.round(preis * 100)) / 100; // Auf zwei Nachkommestellen kuerzen
	}
	/**
	 * @param preis the preis to set
	 * @throws IllegalArgumentException wenn preis <= 0
	 */
	public void setPreis(double preis) {
		if(preis >= 0)
			this.preis = preis;
		else
			throw new IllegalArgumentException("Es muss ein gültiger Preis angegeben werden!");
	}
	/**
	 * @return the beschreibung
	 * @see events.Ebvent#htmlfiler(String)
	 */
	public String getBeschreibung() {
		return htmlfilter(beschreibung);
	}
	/**
	 * @param beschreibung the beschreibung to set
	 * @throws IllegalArgumentException wenn beschreibung == "" oder null
	 */
	public void setBeschreibung(String beschreibung) {
		beschreibung = htmlfilter(beschreibung);
		if(beschreibung == "" || beschreibung == null)
			throw new IllegalArgumentException("Es muss eine gültige Beschreibung angegeben werden!");
		else{
			this.beschreibung = beschreibung;
		}
	}
	/**
	 * @return the beginnzeit
	 * @see events.Ebvent#htmlfiler(String)
	 */
	public String getBeginnzeit() {
		return htmlfilter(beginnzeit);
	}
	/**
	 * @param beginnzeit the beginnzeit to set
	 * @throws IllegalArgumentException wenn beginnzeit == "" oder null
	 */
	public void setBeginnzeit(String beginnzeit) {
		if(beginnzeit == "" || beginnzeit == null)
			throw new IllegalArgumentException("Es muss eine Zeit angegeben werden!");
		else{
			this.beginnzeit = beginnzeit;
		}
		
	}
	/**
	 * @return the teilnehmer als Liste
	 */
	public ArrayList<RegUser> getTeilnehmer() {
		if(teilnehmer == null) teilnehmer = new ArrayList<RegUser>();
		return teilnehmer;
	}
	/**
	 * @param teilnehmer the teilnehmer to set
	 */
	public void setTeilnehmer(ArrayList<RegUser> teilnehmer) {
		this.teilnehmer = teilnehmer;
	}
	/**
	 * @return the datum
	 */
	public Date getDatum() {
		return datum;
	}
	/**
	 * @param datum the datum to set
	 * @throws IllegalArgumentException wenn datum == null oder in der Verangenheit
	 */
	public void setDatum(Date datum) {
		
		Date today = new Date();
		
		if(datum == null)
			throw new IllegalArgumentException("Es muss eine Eventdatum angegeben werden!");
		else{
			if(datum.before(today)){
				throw new IllegalArgumentException("Eventdatum kann nicht in der Vergangenheit sein!");
			}else{
				this.datum = datum;
			}
			
		}
		this.datum = datum;

	}
	/**
	 * @return the veranstalter
	 */
	public String getVeranstalter() {
		return veranstalter;
	}
	/**
	 * @param veranstalter the veranstalter to set
	 */
	public void setVeranstalter(String veranstalter) {
		this.veranstalter = veranstalter;
	}

	
	@Override
	/**
	 * Methode um im spaeteren Velauf die Klasse sortierbar zu machen.
	 */
	public int compareTo(Event event) {
		return this.getDatum().compareTo(event.getDatum());
	}
	/**
	 * 
	 * @return Datum als String
	 */
	public String getDatumToString() {
		// Datum im Format TT.MM.YYYY zurückgeben
		String str_datum;
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		str_datum = df.format(datum);
		return str_datum;
	}
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getLocationToString() throws IOException {
		// Locationnamen zurueckgeben
		LocationManagementDAO lmdao = new LocationManagementDAO();
		ArrayList<Location> al_loc = lmdao.getLocationList();
		
		String LocationName = "";
		for(int i = 0; i < al_loc.size(); i++){
			if(al_loc.get(i).getId() == location_id){
				LocationName = al_loc.get(i).getName();
			}
		}
		return LocationName;
	}
	/**
	 * 
	 * @param username
	 * @return true wenn user teilnimmt
	 */
	public boolean nimmtTeil(String username){
		ArrayList<RegUser> al_teil = getTeilnehmer();
		boolean nt = false;
		
		for(int i = 0; i < al_teil.size(); i++){
			if(al_teil.get(i).getUsername().equals(username)){
				nt = true;
			}
		}
		return nt;
	}
	/**
	 * 
	 * @param username
	 * @return true wenn user Veranstalter ist
	 * @throws IOException
	 */
	public boolean isVeranstalter(String username) throws IOException{
		
		if(username.equals(this.veranstalter)) return true;
		return false;
	}
	/**
	 * 
	 * @return anzahl der teilnehmer
	 */
	public int getTeilnehmerAnzahl(){
		return getTeilnehmer().size();
	}
	/**
	 * 
	 * @return Vor- und Nachname des Veranstalterws
	 * @throws IOException
	 */
	public String getVeranstalterName() throws IOException{
		// "Vorname Nachname"
		
		UserManagementDAO umdao = new UserManagementDAO();
		ArrayList<RegUser> al_regu = umdao.getUserList();
		String str_veranstalter = null;
		
		for(int i = 0; i < al_regu.size(); i++){
			if(al_regu.get(i).getUsername().equals(this.veranstalter)){
				str_veranstalter = al_regu.get(i).getFullName();
			}
		}		
		return str_veranstalter;
	}
	/**
	 * 
	 * @return Adresse von der Location
	 * @throws IOException
	 */
	public String getAdresse() throws IOException{
		// Adresse der Location zurueckgeben, in der das Event stattfindet
		
		LocationManagementDAO lmdao = new LocationManagementDAO();
		ArrayList<Location> al_loc = lmdao.getLocationList();
		String adresse = null;
		
		for(int i = 0; i < al_loc.size(); i++){
			if(al_loc.get(i).getId() == this.location_id){
				adresse = al_loc.get(i).getAdresse();
			}
		}
		
		return htmlfilter(adresse);
	}
	/**
	 * 
	 * @param text
	 * @return ersetzt > und < durch &lt; sowie &gt;
	 */
	private String htmlfilter(String text) {
		text = text.replaceAll("<","&lt;");
		text = text.replaceAll(">","&gt;");
		return text;
	}
	/**
	 * 
	 * @param text
	 * @return true wenn Sonderzeichen im text
	 */
	private boolean filter(String text) {
		if(text.contains("<") || text.contains(">") || text.contains("!") || text.contains("?") || text.contains("'") || text.contains("\"") || text.contains("#")
				|| text.contains("(") || text.contains(")") || text.contains("[") || text.contains("]") || text.contains("/") || text.contains("\\") || text.contains("€")
				|| text.contains("{") || text.contains("}") || text.contains("&") || text.contains("%") || text.contains("$") || text.contains("§") || text.contains("~")
				|| text.contains("*") || text.contains("+"))
			return true;
		return false;
	}
}