/**
 * 
 */
package locations;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import users.RegUser;
import dao.UserManagementDAO;

/**
 * @author Dominic Boehm 
 * @author James Patrick Salazar
 * Klasse zur Datenhaltung der Location
 */
@SuppressWarnings("serial")

public class Location implements Comparable<Location>, Serializable {

	private int id;
	private String name;
	private int maxTeilnehmer;
	private String type;
	private String beschreibung;
	private double preis;
	private String adresse;
	private String besitzer;
	
	
	
	/**
	 * Konstruktor zum Initialsieren aller Attribute einer Location
	 * @param id
	 * @param name
	 * @param maxTeilnehmer
	 * @param type
	 * @param beschreibung
	 * @param preis
	 * @param adresse
	 */
	public Location(int id, String name, int maxTeilnehmer, String type,
			String beschreibung, double preis, String adresse, String username) {
		
		setId(id);
		setName(name);
		setMaxTeilnehmer(maxTeilnehmer);
		setType(type);
		setBeschreibung(beschreibung);
		setPreis(preis);
		setAdresse(adresse);
		setBesitzer(username);
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return htmlfilter(name);
	}
	/**
	 * @param name the name to set
	 * @throws IllegalArgumentException wenn name ist null oder ""
	 */
	public void setName(String name) {
		if(name == "" || name == null)
			throw new IllegalArgumentException("Es muss ein Locationname angegeben werden!");
		else{
			this.name = name;
		}
	}
	/**
	 * @return the maxTeilnehmer
	 */
	public int getMaxTeilnehmer() {
		return maxTeilnehmer;
	}
	/**
	 * @param maxTeilnehmer the maxTeilnehmer to set
	 * @throws IllegalArgumentException wenn maxTeilnehmer ist < 0
	 */
	public void setMaxTeilnehmer(int maxTeilnehmer) {
		if(maxTeilnehmer > 0)
			this.maxTeilnehmer = maxTeilnehmer;
		else
			throw new IllegalArgumentException("Es muss eine max. Teilnehmeranzahl angegeben werden!");
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return htmlfilter(type);
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * @return the beschreibung
	 */
	public String getBeschreibung() {
		return htmlfilter(beschreibung);
	}
	/**
	 * @param beschreibung the beschreibung to set
	 * @throws IllegalArgumentException wenn beschreibung ist null oder  ""
	 */
	public void setBeschreibung(String beschreibung) {
		if(beschreibung == "" || beschreibung == null)
			throw new IllegalArgumentException("Es muss eine Beschreibung angegeben werden!");
		else{
			this.beschreibung = beschreibung;
		}
	}
	/**
	 * @return the preis
	 */
	public double getPreis() {
		return preis;
	}
	/**
	 * @param preis the preis to set
	 * @throws IllegalArgumentException wenn preis ist < 0
	 */
	public void setPreis(double preis) {
		if(preis >= 0)
			this.preis = preis;
		else
			throw new IllegalArgumentException("Es muss ein Preis angegeben werden!");	}
	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return htmlfilter(adresse);
	}
	/**
	 * @param adresse the adresse to set
	 * @throws IllegalArgumentException wenn adresse ist null oder  ""
	 */
	public void setAdresse(String adresse) {
		if(adresse == "" || adresse == null || filter(adresse))
			throw new IllegalArgumentException("Es muss eine gültige Adresse angegeben werden!");
		else{
			this.adresse = adresse;
		}
	}
	/**
	 * @return the besitzer
	 */
	public String getBesitzer() {
		return besitzer;
	}
	/**
	 * @param besitzer the besitzer to set
	 */
	public void setBesitzer(String besitzer) {
		this.besitzer = besitzer;
	}
	/**
	 * 
	 * @return Besitzer als String
	 * @throws IOException
	 */
	public String getBesitzerToString() throws IOException {
		
		UserManagementDAO umdao = new UserManagementDAO();
		ArrayList<RegUser> al_regu = umdao.getUserList();
		String str_besitzer = null;
		
		for(int i = 0; i < al_regu.size(); i++){
			if(al_regu.get(i).getUsername().equals(this.besitzer)){
				str_besitzer = al_regu.get(i).getFullName();
			}
		}
		
		return htmlfilter(str_besitzer);
	}
	/**
	 * 
	 * @param username
	 * @return true wenn user ist Vermieter
 	 * @throws IOException
	 */
	public boolean isVermieter(String username) throws IOException{
		if(username.equals(this.besitzer)) return true;
		return false;
	}
	@Override
	/**
	 * Methode um Klasse sortierbar zu machen
	 */
	public int compareTo(Location arg0) {
		
		return this.getName().compareTo(arg0.getName());
	}
	/**
	 * 
	 * @param text
	 * @return ersetzt > und < durch &lt; sowie &gt;
	 */
	private String htmlfilter(String text) {
		text = text.replaceAll("<","&lt;");
		text = text.replaceAll(">","&gt;");
		return text;
	}
	/**
	 * 
	 * @param text
	 * @return true wenn Sonderzeichen im text
	 */
	private boolean filter(String text) {
		if(text.contains("<") || text.contains(">") || text.contains("!") || text.contains("?") || text.contains("'") || text.contains("\"") || text.contains("#")
				|| text.contains("(") || text.contains(")") || text.contains("[") || text.contains("]") || text.contains("/") || text.contains("\\") || text.contains("€")
				|| text.contains("{") || text.contains("}") || text.contains("&") || text.contains("%") || text.contains("$") || text.contains("§") || text.contains("~")
				|| text.contains("*") || text.contains("+"))
			return true;
		return false;
	}
}